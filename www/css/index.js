import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'page--bg-orange__content': {
    'backgroundColor': '#ff6722 !important'
  },
  'toolbar--toobar-orange': {
    'backgroundColor': '#ff6722'
  },
  'p': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'align-center': {
    'textAlign': 'center',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }]
  },
  'img-logo': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'white-text': {
    'color': 'white'
  },
  'row-cpf': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 30 }],
    'marginBottom': [{ 'unit': 'px', 'value': 20 }]
  },
  'input-form': {
    'background': 'white',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }],
    'height': [{ 'unit': 'px', 'value': 50 }],
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'input-form input': {
    'width': [{ 'unit': '%H', 'value': 0.8 }],
    'height': [{ 'unit': '%V', 'value': 0.95 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'textAlign': 'center'
  },
  'input-icon img': {
    'float': 'right',
    'marginTop': [{ 'unit': 'px', 'value': 2 }]
  },
  'input-icon': {
    'background': 'white',
    'boxSizing': 'border-box',
    'WebkitBoxSizing': 'border-box',
    'MozBoxSizing': 'border-box',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C2C2C2' }],
    'boxShadow': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': '#EBEBEB' }],
    'MozBoxShadow': '1px 1px 4px #EBEBEB',
    'WebkitBoxShadow': '1px 1px 4px #EBEBEB',
    'borderRadius': '5px',
    'WebkitBorderRadius': '5px',
    'MozBorderRadius': '5px',
    'outline': 'none'
  },
  'arrow-circle-icon': {
    'backgroundImage': 'url(./../img/next.png)',
    'paddingRight': [{ 'unit': 'px', 'value': 36 }],
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': '97%',
    'backgroundColor': 'white',
    'backgroundSize': '30px'
  },
  'margintop-20': {
    'marginTop': [{ 'unit': 'px', 'value': 20 }]
  },
  'btn-entrar': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'backgroundColor': '#bf4610'
  },
  'btn-facebook': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'backgroundColor': '#39579a'
  },
  'btn-border': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'white' }],
    'color': 'white'
  },
  'btn-facebook ons-icon': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'marginRight': [{ 'unit': 'px', 'value': 5 }]
  },
  'login-text': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'textAlign': 'center',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ababab' }],
    'lineHeight': [{ 'unit': 'em', 'value': 0.1 }],
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 0 }]
  },
  'line': {
    'background': '#ff6722',
    'color': '#a7a7a7',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }]
  },
  'divider-text': {
  }
});
