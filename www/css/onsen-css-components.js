import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // !
 * Copyright 2013-2017 ASIAL CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
  // !
 * Copyright 2012 Adobe Systems Inc.;
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
  'html': {
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'body': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitTextSizeAdjust': '100%'
  },
  '*': {
    // stylelint-disable-line selector-no-universal
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'WebkitTapHighlightColor': 'rgba(0, 0, 0, 0)'
  },
  'input': {
    'WebkitUserSelect': 'auto',
    'MsUserSelect': 'auto',
    'userSelect': 'auto',
    'MozUserSelect': 'text'
  },
  'textarea': {
    'WebkitUserSelect': 'auto',
    'MsUserSelect': 'auto',
    'userSelect': 'auto',
    'MozUserSelect': 'text'
  },
  'select': {
    'WebkitUserSelect': 'auto',
    'MsUserSelect': 'auto',
    'userSelect': 'auto',
    'MozUserSelect': 'text'
  },
  'input:active': {
    'outline': 'none'
  },
  'input:focus': {
    'outline': 'none'
  },
  'textarea:active': {
    'outline': 'none'
  },
  'textarea:focus': {
    'outline': 'none'
  },
  'select:active': {
    'outline': 'none'
  },
  'select:focus': {
    'outline': 'none'
  },
  'h1': {
    'fontSize': [{ 'unit': 'px', 'value': 36 }]
  },
  'h2': {
    'fontSize': [{ 'unit': 'px', 'value': 30 }]
  },
  'h3': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }]
  },
  'h4': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  'h5': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  'h6': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  ':root': {
  },
  'page': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'backgroundColor': '#f9f9f9',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'overflowX': 'visible',
    'overflowY': 'hidden',
    'color': '#1f1f21',
    'MsOverflowStyle': 'none'
  },
  'page::-webkit-scrollbar': {
    'display': 'none'
  },
  'page__content': {
    'backgroundColor': '#f9f9f9',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'boxSizing': 'border-box'
  },
  'page__background': {
    'backgroundColor': '#f9f9f9',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'boxSizing': 'border-box'
  },
  'page--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'page--material__content': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': '400'
  },
  'page__content h1': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page__content h2': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page__content h3': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page__content h4': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page__content h5': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page__content h1': {
    'fontSize': [{ 'unit': 'px', 'value': 28 }]
  },
  'page__content h2': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }]
  },
  'page__content h3': {
    'fontSize': [{ 'unit': 'px', 'value': 20 }]
  },
  'page--material__content h1': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page--material__content h2': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page--material__content h3': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page--material__content h4': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page--material__content h5': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'page--material__content h1': {
    'fontSize': [{ 'unit': 'px', 'value': 28 }]
  },
  'page--material__content h2': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }]
  },
  'page--material__content h3': {
    'fontSize': [{ 'unit': 'px', 'value': 20 }]
  },
  'page--material__background': {
    'backgroundColor': '#ffffff'
  },
  ':root': {
    // background color active
  },
  // ~
  name: Switch
  category: Switch
  elements: ons-switch
  markup: |
    <label class="switch">
      <input type="checkbox" class="switch__input">
      <div class="switch__toggle">
        <div class="switch__handle"></div>
      </div>
    </label>
    <label class="switch">
      <input type="checkbox" class="switch__input" checked>
      <div class="switch__toggle">
        <div class="switch__handle"></div>
      </div>
    </label>
    <label class="switch">
      <input type="checkbox" class="switch__input" disabled>
      <div class="switch__toggle">
        <div class="switch__handle"></div>
      </div>
    </label>
  'switch': {
    'display': 'inline-block',
    'verticalAlign': 'top',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'position': 'relative',
    'minWidth': [{ 'unit': 'px', 'value': 51 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 20 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'overflow': 'visible',
    'width': [{ 'unit': 'px', 'value': 51 }],
    'height': [{ 'unit': 'px', 'value': 32 }],
    'zIndex': '0',
    'textAlign': 'left'
  },
  'switch__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'zIndex': '0'
  },
  // switch toggle background
  'switch__toggle': {
    'backgroundColor': '#fff',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '30px',
    'WebkitTransitionProperty': 'all',
    'transitionProperty': 'all',
    'WebkitTransitionDuration': '0.35s',
    'transitionDuration': '0.35s',
    'WebkitTransitionTimingFunction': 'ease-out',
    'transitionTimingFunction': 'ease-out',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': '#e5e5e5' }]
  },
  // switch toggle circle
  'switch__handle': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'position': 'absolute',
    'content': '''',
    'borderRadius': '28px',
    'height': [{ 'unit': 'px', 'value': 28 }],
    'width': [{ 'unit': 'px', 'value': 28 }],
    'backgroundColor': 'white',
    'left': [{ 'unit': 'px', 'value': 1 }],
    'top': [{ 'unit': 'px', 'value': 2 }],
    'WebkitTransitionProperty': 'all',
    'transitionProperty': 'all',
    'WebkitTransitionDuration': '0.35s',
    'transitionDuration': '0.35s',
    'WebkitTransitionTimingFunction': 'cubic-bezier(.59, .01, .5, .99)',
    'transitionTimingFunction': 'cubic-bezier(.59, .01, .5, .99)',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': '#e4e4e4,' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .25)' }]
  },
  'switch--active__handle': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  ':checked + switch__toggle': {
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': '#5198db' }],
    'backgroundColor': '#5198db'
  },
  ':checked + switch__toggle > switch__handle': {
    'left': [{ 'unit': 'px', 'value': 21 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .25)' }]
  },
  ':disabled + switch__toggle': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'switch__touch': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': -5 }],
    'bottom': [{ 'unit': 'px', 'value': -5 }],
    'left': [{ 'unit': 'px', 'value': -10 }],
    'right': [{ 'unit': 'px', 'value': -10 }]
  },
  // ~
  name: Material Switch
  category: Switch
  elements: ons-switch
  markup: |
    <label class="switch switch--material">
      <input type="checkbox" class="switch__input switch--material__input">
      <div class="switch__toggle switch--material__toggle">
        <div class="switch__handle switch--material__handle">
        </div>
      </div>
    </label>
    <label class="switch switch--material">
      <input type="checkbox" class="switch__input switch--material__input" checked>
      <div class="switch__toggle switch--material__toggle">
        <div class="switch__handle switch--material__handle">
        </div>
      </div>
    </label>
    <label class="switch switch--material">
      <input type="checkbox" class="switch__input switch--material__input" disabled>
      <div class="switch__toggle switch--material__toggle">
        <div class="switch__handle switch--material__handle">
        </div>
      </div>
    </label>
  'switch--material': {
    'width': [{ 'unit': 'px', 'value': 36 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }],
    'minWidth': [{ 'unit': 'px', 'value': 36 }]
  },
  'switch--material__toggle': {
    'backgroundColor': '#b0afaf',
    'marginTop': [{ 'unit': 'px', 'value': 5 }],
    'height': [{ 'unit': 'px', 'value': 14 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'switch--material__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'zIndex': '0'
  },
  'switch--material__handle': {
    'backgroundColor': '#f1f1f1',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': -5 }],
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }]
  },
  ':checked + switch--material__toggle': {
    'backgroundColor': '#77c2bb',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  ':checked + switch--material__toggle > switch--material__handle': {
    'left': [{ 'unit': 'px', 'value': 16 }],
    'backgroundColor': '#009688',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }]
  },
  ':disabled + switch--material__toggle': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'switch--material__handle:before': {
    'background': 'transparent',
    'content': '''',
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'borderRadius': '50%',
    'zIndex': '0',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }],
    'WebkitTransition': 'box-shadow 0.1s linear',
    'transition': 'box-shadow 0.1s linear'
  },
  'switch--material__toggle > switch--active__handle:before': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }]
  },
  ':checked + switch--material__toggle > switch--active__handle:before': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'string', 'value': 'alpha(#009688, 0.2)' }]
  },
  'switch--material__touch': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': -10 }],
    'bottom': [{ 'unit': 'px', 'value': -10 }],
    'left': [{ 'unit': 'px', 'value': -15 }],
    'right': [{ 'unit': 'px', 'value': -15 }]
  },
  // ~
  name: Range
  category: Range
  elements: ons-range
  markup: |
    <div class="range">
      <input type="range" class="range__input">
    </div>

    <div class="range range--disabled">
      <input type="range" class="range__input" disabled>
    </div>
  'range': {
    'display': 'inline-block',
    'width': [{ 'unit': 'px', 'value': 100 }],
    'height': [{ 'unit': 'px', 'value': 32 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'backgroundImage': '-webkit-linear-gradient(#ccc, #ccc)',
    'backgroundImage': 'linear-gradient(#ccc, #ccc)',
    'backgroundPosition': 'left center',
    'backgroundSize': '100% 2px',
    'backgroundRepeat': 'no-repeat',
    'backgroundColor': 'transparent',
    'overflow': 'visible'
  },
  'range__input': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'backgroundImage': '-webkit-linear-gradient(rgba(24, 103, 194, .81), rgba(24, 103, 194, .81))',
    'backgroundImage': 'linear-gradient(rgba(24, 103, 194, .81), rgba(24, 103, 194, .81))',
    'backgroundPosition': 'left center',
    'backgroundSize': '0% 2px',
    'backgroundRepeat': 'no-repeat',
    'overflow': 'hidden',
    'height': [{ 'unit': 'px', 'value': 32 }],
    'position': 'relative',
    'zIndex': '1',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'range__input::-moz-range-track': {
    'position': 'relative',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'background': 'none',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'range__input::-ms-track': {
    'position': 'relative',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': '#ccc',
    'height': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '30px'
  },
  'range__input::-webkit-slider-thumb': {
    'cursor': 'pointer',
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 29 }],
    'width': [{ 'unit': 'px', 'value': 29 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    // TODO: Add shadow
    // box-shadow: 0 0 0 1px #e4e4e4, 0 3px 2px rgba(0, 0, 0, 0.25);
    'borderRadius': '30px',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'boxSizing': 'border-box',
    'WebkitAppearance': 'none',
    'appearance': 'none',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '1'
  },
  'range__input::-moz-range-thumb': {
    'cursor': 'pointer',
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 29 }],
    'width': [{ 'unit': 'px', 'value': 29 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    // TODO: Add shadow
    // box-shadow: 0 0 0 1px #e4e4e4, 0 3px 2px rgba(0, 0, 0, 0.25);
    'borderRadius': '30px',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'range__input::-ms-thumb': {
    'cursor': 'pointer',
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 29 }],
    'width': [{ 'unit': 'px', 'value': 29 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    // TODO: Add shadow
    // box-shadow: 0 0 0 1px #e4e4e4, 0 3px 2px rgba(0, 0, 0, 0.25);
    'borderRadius': '30px',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'range__input::-ms-fill-lower': {
    'height': [{ 'unit': 'px', 'value': 2 }],
    'backgroundColor': 'rgba(24, 103, 194, .81)'
  },
  'range__input::-ms-tooltip': {
    'display': 'none'
  },
  'range__input:disabled': {
    'opacity': '1',
    'pointerEvents': 'none'
  },
  'range--disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'pointerEvents': 'none'
  },
  // ~
  name: Material Range
  category: Range
  elements: ons-range
  markup: |
    <div class="range range--material">
      <input type="range" class="range__input range--material__input">
    </div>

    <div class="range range--material range--disabled">
      <input type="range" class="range__input range--material__input" disabled>
    </div>
  'range--material': {
    'backgroundImage': '-webkit-linear-gradient(#e0e0e0, #e0e0e0)',
    'backgroundImage': 'linear-gradient(#e0e0e0, #e0e0e0)'
  },
  'range--material__input': {
    'backgroundImage': '-webkit-linear-gradient(#009688, #009688)',
    'backgroundImage': 'linear-gradient(#009688, #009688)',
    'backgroundPosition': 'center left',
    'backgroundSize': '0% 2px',
    'overflow': 'visible'
  },
  'range--material__input::-webkit-slider-thumb': {
    'boxSizing': 'border-box',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'height': [{ 'unit': 'px', 'value': 36 }],
    'width': [{ 'unit': 'px', 'value': 14 }],
    'borderRadius': '0',
    'backgroundColor': 'transparent',
    'backgroundImage': '-webkit-radial-gradient(circle cover, #009688 0%, #009688 6.6px, transparent 7.2px)',
    'backgroundImage': 'radial-gradient(circle cover, #009688 0%, #009688 6.6px, transparent 7.2px)'
  },
  'range--material__input::-moz-range-thumb': {
    'boxSizing': 'border-box',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'height': [{ 'unit': 'px', 'value': 36 }],
    'width': [{ 'unit': 'px', 'value': 14 }],
    'borderRadius': '0',
    'backgroundColor': 'transparent',
    'backgroundImage': '-moz-radial-gradient(circle cover, #009688 0%, #009688 6.6px, transparent 7.2px)'
  },
  'range--material__input:focus::-moz-range-thumb': {
    'boxSizing': 'border-box',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'height': [{ 'unit': 'px', 'value': 36 }],
    'width': [{ 'unit': 'px', 'value': 14 }],
    'borderRadius': '0',
    'backgroundColor': 'transparent',
    'backgroundImage': '-moz-radial-gradient(circle cover, #009688 0%, #009688 6.6px, transparent 7.2px)'
  },
  'range--material__input::-moz-range-track': {
    'background': 'none'
  },
  'range--material__input::-ms-thumb': {
    'marginTop': [{ 'unit': 'px', 'value': 1 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'height': [{ 'unit': 'px', 'value': 12 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'borderRadius': '100%',
    'borderColor': '#009688',
    'backgroundColor': '#009688'
  },
  'range--material__input::-ms-fill-lower': {
    'backgroundColor': '#009688'
  },
  'range--material__input::-ms-fill-upper': {
    'marginTop': [{ 'unit': 'px', 'value': -6 }],
    'marginLeft': [{ 'unit': 'px', 'value': -6 }],
    'content': '''',
    'display': 'block',
    'borderWidth': '0',
    'borderRadius': '100%',
    'height': [{ 'unit': 'px', 'value': 12 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'backgroundColor': '#009688',
    'opacity': '0.2',
    'WebkitTransition': '-webkit-transform 0.1s linear',
    'transition': '-webkit-transform 0.1s linear',
    'transition': 'transform 0.1s linear',
    'transition': 'transform 0.1s linear, -webkit-transform 0.1s linear'
  },
  'range--material__input::-moz-range-thumb:before': {
    'display': 'none'
  },
  'range--material__input::-webkit-slider-thumb:before': {
    'display': 'none'
  },
  'range--material__input::-moz-range-thumb:after': {
    'marginTop': [{ 'unit': 'px', 'value': -3 }],
    'marginLeft': [{ 'unit': 'px', 'value': -3 }],
    'content': '''',
    'display': 'block',
    'borderWidth': '0',
    'borderRadius': '100%',
    'height': [{ 'unit': 'px', 'value': 12 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'backgroundColor': '#009688',
    'opacity': '0.2',
    'WebkitTransition': '-webkit-transform 0.1s linear',
    'transition': '-webkit-transform 0.1s linear',
    'transition': 'transform 0.1s linear',
    'transition': 'transform 0.1s linear, -webkit-transform 0.1s linear'
  },
  'range--material__input::-webkit-slider-thumb:after': {
    'content': '''',
    'display': 'block',
    'borderWidth': '0',
    'borderRadius': '100%',
    'height': [{ 'unit': 'px', 'value': 12 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'backgroundColor': '#009688',
    'opacity': '0.2',
    'WebkitTransition': '-webkit-transform 0.1s linear',
    'transition': '-webkit-transform 0.1s linear',
    'transition': 'transform 0.1s linear',
    'transition': 'transform 0.1s linear, -webkit-transform 0.1s linear',
    'display': 'inline-block',
    'marginLeft': [{ 'unit': 'px', 'value': 1 }],
    'marginTop': [{ 'unit': 'px', 'value': 12 }]
  },
  'range--material__input:active::-webkit-slider-thumb:after': {
    'WebkitTransform': 'scale(2.5)',
    'transform': 'scale(2.5)'
  },
  'range--material__input:active::-moz-range-thumb:after': {
    'transform': 'scale(2.5)'
  },
  'range--material__input:active::-ms-fill-upper': {
    'transform': 'scale(2.5)'
  },
  // ~
  name: Notification
  category: Notification
  markup: |
    <span class="notification">1</span>
    <span class="notification">10</span>
    <span class="notification">999</span>
  'notification': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'textDecoration': 'none',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'height': [{ 'unit': 'px', 'value': 19 }],
    'borderRadius': '19px',
    'backgroundColor': '#dc5236',
    'color': 'white',
    'textAlign': 'center',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'minWidth': [{ 'unit': 'px', 'value': 19 }],
    'lineHeight': [{ 'unit': 'px', 'value': 19 }],
    'fontWeight': '400'
  },
  'notification:empty': {
    'display': 'none'
  },
  // ~
  name: Toolbar
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar">
      <div class="toolbar__center">Navigation Bar</div>
    </div>
  'toolbar': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'wordSpacing': '0',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'zIndex': '2',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitBoxAlign': 'stretch',
    'MsFlexAlign': 'stretch',
    'alignItems': 'stretch',
    'MsFlexWrap': 'nowrap',
    'flexWrap': 'nowrap',
    'height': [{ 'unit': 'px', 'value': 44 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }],
    'background': '#fff',
    'color': '#1f1f21',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'fontWeight': '400',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'whiteSpace': 'nowrap',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #bbb, #bbb 100%)',
    'backgroundImage': 'linear-gradient(0deg, #bbb, #bbb 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #bbb, #bbb 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #bbb, #bbb 50%, transparent 50%)'
    }
  },
  'toolbar__bg': {
    'background': '#fff'
  },
  'toolbar__item': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'height': [{ 'unit': 'px', 'value': 44 }],
    'overflow': 'visible',
    'display': 'block',
    'verticalAlign': 'middle'
  },
  'toolbar__left': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'maxWidth': [{ 'unit': '%H', 'value': 0.5 }],
    'width': [{ 'unit': '%H', 'value': 0.27 }],
    'textAlign': 'left',
    'lineHeight': [{ 'unit': 'px', 'value': 44 }]
  },
  'toolbar__right': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'maxWidth': [{ 'unit': '%H', 'value': 0.5 }],
    'width': [{ 'unit': '%H', 'value': 0.27 }],
    'textAlign': 'right',
    'lineHeight': [{ 'unit': 'px', 'value': 44 }]
  },
  'toolbar__center': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'width': [{ 'unit': '%H', 'value': 0.46 }],
    'textAlign': 'center',
    'lineHeight': [{ 'unit': 'px', 'value': 44 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'color': '#1f1f21'
  },
  'toolbar__title': {
    'lineHeight': [{ 'unit': 'px', 'value': 44 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'color': '#1f1f21',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'visible'
  },
  'toolbar__center:first-child:last-child': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  // ~
  name: Toolbar Item
  category: Toolbar
  elements: ons-toolbar ons-toolbar-button
  markup: |
    <div class="toolbar">
      <div class="toolbar__left">
        <span class="toolbar-button">
          <i class="ion-navicon" style="font-size:32px; vertical-align:-6px;"></i>
        </span>
      </div>

      <div class="toolbar__center">
        Navigation Bar
      </div>

      <div class="toolbar__right">
        <span class="toolbar-button">Label</span>
      </div>
    </div>
  // ~
  name: Toolbar with Outline Button
  category: Toolbar
  elements: ons-toolbar ons-toolbar-button
  markup: |
    <!-- Prerequisite=This example use ionicons(http://ionicons.com) to display icons. -->
    <div class="toolbar">
      <div class="toolbar__left">
        <span class="toolbar-button toolbar-button--outline">
          <i class="ion-navicon" style="font-size:32px; vertical-align:-6px;"></i>
        </span>
      </div>

      <div class="toolbar__center">
        Title
      </div>

      <div class="toolbar__right">
        <span class="toolbar-button toolbar-button--outline">Button</span>
      </div>
    </div>
  // ~
  name: Bottom Bar
  category: Toolbar
  elements: ons-bottom-toolbar
  markup: |
    <div class="bottom-bar">
      <div class="bottom-bar__line-height" style="text-align:center">Bottom Toolbar</div>
    </div>
  'bottom-bar': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'wordSpacing': '0',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'zIndex': '2',
    'display': 'block',
    'height': [{ 'unit': 'px', 'value': 44 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }],
    'background': '#fff',
    'color': '#1f1f21',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'fontWeight': '400',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#bbb' }],
    'position': 'absolute',
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'top',
    'backgroundImage': '-webkit-linear-gradient(top, #bbb, #bbb 100%)',
    'backgroundImage': 'linear-gradient(180deg, #bbb, #bbb 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(top, #bbb, #bbb 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(180deg, #bbb, #bbb 50%, transparent 50%)'
    }
  },
  'bottom-bar__line-height': {
    'lineHeight': [{ 'unit': 'px', 'value': 44 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'bottom-bar--transparent': {
    'backgroundColor': 'transparent',
    'backgroundImage': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: Toolbar with Segment
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar">
      <div class="toolbar__center">
        <div class="segment" style="width:200px;margin:7px 50px;">
          <div class="segment__item">
            <input type="radio" class="segment__input" name="navi-segment-a" checked>
            <div class="segment__button">One</div>
          </div>

          <div class="segment__item">
            <input type="radio" class="segment__input" name="navi-segment-a">
            <div class="segment__button">Two</div>
          </div>
        </div>
      </div>
    </div>
  // ~
  name: Material Toolbar
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar toolbar--material">
      <div class="toolbar__center toolbar--material__center">
        Title
      </div>
    </div>
  'toolbar--material': {
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'MsFlexWrap': 'nowrap',
    'flexWrap': 'nowrap',
    'WebkitBoxPack': 'justify',
    'MsFlexPack': 'justify',
    'justifyContent': 'space-between',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'borderBottom': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .3)' }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'backgroundColor': '#009688',
    'backgroundSize': '0'
  },
  // ~
  name: No Shadow Toolbar
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar toolbar--noshadow">
      <div class="toolbar__left">
        <span class="toolbar-button">
          <i class="ion-navicon" style="font-size:32px; vertical-align:-6px;"></i>
        </span>
      </div>
      <div class="toolbar__center">
        Navigation Bar
      </div>
      <div class="toolbar__right">
        <span class="toolbar-button">Label</span>
      </div>
    </div>
  'toolbar--noshadow': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }]
  },
  'toolbar--material__left': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'fontWeight': '500',
    'color': '#ffffff',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'minWidth': [{ 'unit': 'px', 'value': 72 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'lineHeight': [{ 'unit': 'px', 'value': 56 }]
  },
  'toolbar--material__right': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'fontWeight': '500',
    'color': '#ffffff',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'minWidth': [{ 'unit': 'px', 'value': 72 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'lineHeight': [{ 'unit': 'px', 'value': 56 }]
  },
  'toolbar--material__center': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'fontWeight': '500',
    'color': '#ffffff',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'WebkitBoxFlex': '1',
    'MsFlexPositive': '1',
    'flexGrow': '1',
    'overflow': 'hidden',
    'textOverflow': 'ellipsis',
    'textAlign': 'left',
    'lineHeight': [{ 'unit': 'px', 'value': 56 }]
  },
  'toolbar--material__center:first-child': {
    'marginLeft': [{ 'unit': 'px', 'value': 16 }]
  },
  'toolbar--material__center:last-child': {
    'marginRight': [{ 'unit': 'px', 'value': 16 }]
  },
  'toolbar--material__left:empty': {
    'minWidth': [{ 'unit': 'px', 'value': 16 }]
  },
  'toolbar--material__right:empty': {
    'minWidth': [{ 'unit': 'px', 'value': 16 }]
  },
  // ~
  name: Material Toolbar with Icons
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar toolbar--material">
      <div class="toolbar__left toolbar--material__left">
        <span class="toolbar-button toolbar-button--material">
          <i class="zmdi zmdi-menu"></i>
        </span>
      </div>
      <div class="toolbar__center toolbar--material__center">
        Title
      </div>
      <div class="toolbar__right toolbar--material__right">
        <span class="toolbar-button toolbar-button--material">
          <i class="zmdi zmdi-search"></i>
        </span>
        <span class="toolbar-button toolbar-button--material">
          <i class="zmdi zmdi-favorite"></i>
        </span>
        <span class="toolbar-button toolbar-button--material">
          <i class="zmdi zmdi-more-vert"></i>
        </span>
      </div>
    </div>
  // ~
  name: Transparent Toolbar
  category: Toolbar
  elements: ons-toolbar
  markup: |
    <div class="toolbar toolbar--transparent">
      <div class="toolbar__left">
        <span class="toolbar-button">
          <i class="ion-navicon" style="font-size:32px; vertical-align:-6px;"></i>
        </span>
      </div>
      <div class="toolbar__center">
        Navigation Bar
      </div>
      <div class="toolbar__right">
        <span class="toolbar-button">Label</span>
      </div>
    </div>
  'toolbar--transparent': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button">Button</button>
    <button class="button" disabled>Button</button>
  'button': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button:active': {
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2'
  },
  'button:focus': {
    'outline': '0'
  },
  'button:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // ~
  name: Outline Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button button--outline">Button</button>
    <button class="button button--outline" disabled>Button</button>
  'button--outline': {
    'backgroundColor': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'color': 'rgba(24, 103, 194, .81)'
  },
  'button--outline:active': {
    'backgroundColor': 'rgba(203, 221, 241, 0.9430000000000001)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'color': 'rgba(24, 103, 194, .81)',
    'opacity': '1'
  },
  'button--outline:hover': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'WebkitTransition': '0',
    'transition': '0'
  },
  // ~
  name: Light Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button button--light">Button</button>
    <button class="button button--light" disabled>Button</button>
  'button--light': {
    'backgroundColor': 'transparent',
    'color': 'rgba(0, 0, 0, .4)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }]
  },
  'button--light:active': {
    'backgroundColor': 'rgba(0, 0, 0, .05)',
    'color': 'rgba(0, 0, 0, .4)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }],
    'opacity': '1'
  },
  // ~
  name: Quiet Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button--quiet">Button</button>
    <button class="button--quiet" disabled>Button</button>
  'button--quiet': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'background': 'transparent',
    'color': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button--quiet:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button--quiet[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button--quiet:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button--quiet:focus': {
    'outline': '0'
  },
  'button--quiet:active': {
    'backgroundColor': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2',
    'color': 'rgba(24, 103, 194, .81)'
  },
  // ~
  name: Call To Action Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button--cta">Button</button>
    <button class="button--cta" disabled>Button</button>
  'button--cta': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': '#25a6d9',
    'color': 'white'
  },
  'button--cta:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button--cta:focus': {
    'outline': '0'
  },
  'button--cta:active': {
    'color': 'white',
    'backgroundColor': '#25a6d9',
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2'
  },
  'button--cta:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--cta[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // name: Large Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button button--large" style="width: 95%; margin: 0 auto;">Button</button>
  'button--large': {
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }],
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'textAlign': 'center'
  },
  'button--large:active': {
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2',
    'transition': 'none'
  },
  'button--large:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--large[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--large:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button--large:focus': {
    'outline': '0'
  },
  // ~
  name: Large Quiet Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button--large--quiet" style="width: 95%; margin: 0 auto;">Button</button>
  'button--large--quiet': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }],
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'color': 'rgba(24, 103, 194, .81)',
    'textAlign': 'center'
  },
  'button--large--quiet:active': {
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2',
    'color': 'rgba(24, 103, 194, .81)',
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'button--large--quiet:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--large--quiet[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--large--quiet:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button--large--quiet:focus': {
    'outline': '0'
  },
  // ~
  name: Large Call To Action Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button--large--cta" style="width: 95%; margin: 0 auto;">Button</button>
  'button--large--cta': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': '#25a6d9',
    'color': 'white',
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'textAlign': 'center',
    'display': 'block'
  },
  'button--large--cta:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button--large--cta:focus': {
    'outline': '0'
  },
  'button--large--cta:active': {
    'color': 'white',
    'backgroundColor': '#25a6d9',
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2'
  },
  'button--large--cta:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button--large--cta[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // ~
  name: Material Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button button--material">BUTTON</button>
    <button class="button button--material" disabled>DISABLED</button>
  'button--material': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }],
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'minHeight': [{ 'unit': 'px', 'value': 36 }],
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'WebkitTransition': 'box-shadow 0.2s ease',
    'transition': 'box-shadow 0.2s ease',
    'textAlign': 'center',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'WebkitTransform': 'translate3d(0, 0, 0)',
    'transform': 'translate3d(0, 0, 0)',
    // text-transform: uppercase;
    'backgroundColor': '#009688',
    'color': '#ffffff',
    'fontWeight': '500'
  },
  'button--material:active': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 18 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }],
    'backgroundColor': '#bf4610',
    'opacity': '1'
  },
  'button--material:focus': {
    'outline': '0'
  },
  'button--material:disabled': {
    'opacity': '0.5',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'button--material[disabled]': {
    'opacity': '0.5',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: Material Flat Button
  category: Button
  elements: ons-button
  markup: |
    <button class="button button--material--flat">BUTTON</button>
    <button class="button button--material--flat" disabled>DISABLED</button>
  'button--material--flat': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'white',
    'verticalAlign': 'middle',
    'backgroundColor': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '3px',
    'WebkitTransition': 'none',
    'transition': 'none',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }],
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'minHeight': [{ 'unit': 'px', 'value': 36 }],
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'WebkitTransition': 'box-shadow 0.2s ease',
    'transition': 'box-shadow 0.2s ease',
    'textAlign': 'center',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'WebkitTransform': 'translate3d(0, 0, 0)',
    'transform': 'translate3d(0, 0, 0)',
    // text-transform: uppercase;
    'backgroundColor': '#009688',
    'color': '#ffffff',
    'fontWeight': '500',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#009688'
  },
  'button--material--flat:active': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#009688',
    'outline': '0',
    'opacity': '1',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button--material--flat:focus': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#009688',
    'outline': '0',
    'opacity': '1',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button--material--flat:disabled': {
    'opacity': '0.5',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'button--material--flat[disabled]': {
    'opacity': '0.5',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: Button Bar
  category: Segment
  markup: |
    <div class="button-bar" style="width:280px;">
      <div class="button-bar__item">
        <button class="button-bar__button">One</button>
      </div>
      <div class="button-bar__item">
        <button class="button-bar__button">Two</button>
      </div>
      <div class="button-bar__item">
        <button class="button-bar__button">Three</button>
      </div>
    </div>
  'button-bar': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': '-webkit-inline-box',
    'display': '-ms-inline-flexbox',
    'display': 'inline-flex',
    'WebkitBoxAlign': 'stretch',
    'MsFlexAlign': 'stretch',
    'alignItems': 'stretch',
    'MsFlexLinePack': 'stretch',
    'alignContent': 'stretch',
    'MsFlexWrap': 'nowrap',
    'flexWrap': 'nowrap',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'button-bar__item': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'borderRadius': '0',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'position': 'relative',
    'overflow': 'hidden',
    'boxSizing': 'border-box'
  },
  'button-bar__button': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'borderRadius': '0',
    'backgroundColor': 'transparent',
    'color': 'rgba(18, 114, 224, .77)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderTopWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderBottomWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRightWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderLeftWidth': [{ 'unit': 'px', 'value': 0 }],
    'fontWeight': '400',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'height': [{ 'unit': 'px', 'value': 27 }],
    'lineHeight': [{ 'unit': 'px', 'value': 27 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'background-color 0.2s linear, color 0.2s linear',
    'transition': 'background-color 0.2s linear, color 0.2s linear',
    'boxSizing': 'border-box'
  },
  'button-bar__button:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'button-bar__button:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button-bar__button:focus': {
    'outline': '0'
  },
  ':checked + button-bar__button': {
    'backgroundColor': 'rgba(18, 114, 224, .77)',
    'color': '#fff',
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button-bar__button:active': {
    'backgroundColor': 'rgba(205, 225, 248, 0.931)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderRight': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  ':active + button-bar__button': {
    'backgroundColor': 'rgba(205, 225, 248, 0.931)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderRight': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'button-bar__item:first-child > button-bar__button': {
    'borderLeftWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRadius': '4px 0 0 4px'
  },
  'button-bar__item:last-child > button-bar__button': {
    'borderRightWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRadius': '0 4px 4px 0'
  },
  // ~
  name: Segment
  category: Segment
  markup: |
    <div class="segment" style="width: 280px; margin: 0 auto;">
      <div class="segment__item">
        <input type="radio" class="segment__input" name="segment-a" checked>
        <button class="segment__button">One</button>
      </div>
      <div class="segment__item">
        <input type="radio" class="segment__input" name="segment-a">
        <button class="segment__button">Two</button>
      </div>
      <div class="segment__item">
        <input type="radio" class="segment__input" name="segment-a">
        <button class="segment__button">Three</button>
      </div>
    </div>
  'segment': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': '-webkit-inline-box',
    'display': '-ms-inline-flexbox',
    'display': 'inline-flex',
    'WebkitBoxAlign': 'stretch',
    'MsFlexAlign': 'stretch',
    'alignItems': 'stretch',
    'MsFlexLinePack': 'stretch',
    'alignContent': 'stretch',
    'MsFlexWrap': 'nowrap',
    'flexWrap': 'nowrap',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'segment__item': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'borderRadius': '0',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'position': 'relative',
    'overflow': 'hidden',
    'boxSizing': 'border-box'
  },
  'segment__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none'
  },
  'segment__button': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'borderRadius': '0',
    'backgroundColor': 'transparent',
    'color': 'rgba(18, 114, 224, .77)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderTopWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderBottomWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRightWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderLeftWidth': [{ 'unit': 'px', 'value': 0 }],
    'fontWeight': '400',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'height': [{ 'unit': 'px', 'value': 29 }],
    'lineHeight': [{ 'unit': 'px', 'value': 29 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'background-color 0.2s linear, color 0.2s linear',
    'transition': 'background-color 0.2s linear, color 0.2s linear',
    'boxSizing': 'border-box'
  },
  'segment__button:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'segment__button:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'segment__button:focus': {
    'outline': '0'
  },
  ':active + segment__button': {
    'backgroundColor': 'rgba(205, 225, 248, 0.931)',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'borderRight': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(18, 114, 224, .77)' }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  ':checked + segment__button': {
    'backgroundColor': 'rgba(18, 114, 224, .77)',
    'color': '#fff',
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'segment__item:first-child > segment__button': {
    'borderLeftWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRadius': '4px 0 0 4px'
  },
  'segment__item:last-child > segment__button': {
    'borderRightWidth': [{ 'unit': 'px', 'value': 1 }],
    'borderRadius': '0 4px 4px 0'
  },
  // ~
  name: Material Segment
  category: Segment
  markup: |
    <div class="segment segment--material" style="width: 280px; margin: 0 auto;">
      <div class="segment__item segment--material__item">
        <input type="radio" class="segment__input segment--material__input" name="segment-b" checked>
        <button class="segment__button segment--material__button">One</button>
      </div>
      <div class="segment__item segment--material__item">
        <input type="radio" class="segment__input segment--material__input" name="segment-b">
        <button class="segment__button segment--material__button">Two</button>
      </div>
      <div class="segment__item segment--material__item">
        <input type="radio" class="segment__input segment--material__input" name="segment-b">
        <button class="segment__button segment--material__button">Three</button>
      </div>
    </div>
  'segment--material': {
    'borderRadius': '2px',
    'overflow': 'hidden',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .24)' }]
  },
  'segment--material__button': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'height': [{ 'unit': 'px', 'value': 32 }],
    'lineHeight': [{ 'unit': 'px', 'value': 32 }],
    'borderWidth': '0',
    'color': 'rgba(0, 0, 0, .38)',
    'borderRadius': '0',
    'backgroundColor': '#fafafa'
  },
  'segment--material__item:first-child > segment--material__button': {
    'borderRadius': '0',
    'borderWidth': '0'
  },
  'segment--material__item:last-child > segment--material__button': {
    'borderRadius': '0',
    'borderWidth': '0'
  },
  ':active + segment--material__button': {
    'backgroundColor': '#fafafa',
    'borderRadius': '0',
    'borderWidth': '0',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'WebkitTransition': 'none',
    'transition': 'none',
    'color': 'rgba(0, 0, 0, .38)'
  },
  ':checked + segment--material__button': {
    'backgroundColor': '#c8c8c8',
    'color': '#353535',
    'borderRadius': '0',
    'borderWidth': '0'
  },
  ':root': {
    // Text color
    // Text color active
  },
  // ~
  name: Icon Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <!-- Prerequisite=This example use ionicons(http://ionicons.com) to display icons. -->
    <div class="tabbar">
      <label class="tabbar__item">
        <input type="radio" name="tabbar-a" checked="checked">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-stop"></i>
          <div class="tabbar__label">One</div>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-a">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-record"></i>
          <div class="tabbar__label">Two</div>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-a">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-star"></i>
          <div class="tabbar__label">Three</div>
        </button>
      </label>
    </div>
  // ~
  name: Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar">
      <label class="tabbar__item">
        <input type="radio" name="tabbar-c" checked="checked">
        <button class="tabbar__button">
          <div class="tabbar__label">One</div>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-c">
        <button class="tabbar__button">
          <div class="tabbar__label">Two</div>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-c">
        <button class="tabbar__button">
          <div class="tabbar__label">Three</div>
        </button>
      </label>
    </div>
  'tabbar': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': 'table',
    'tableLayout': 'fixed',
    'position': 'absolute',
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'whiteSpace': 'nowrap',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 49 }],
    'backgroundColor': '#fff',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderTop': [{ 'unit': 'string', 'value': 'none' }],
      'backgroundSize': '100% 1px',
      'backgroundRepeat': 'no-repeat',
      'backgroundPosition': 'top',
      'backgroundImage': '-webkit-linear-gradient(top, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(180deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'tabbar__item': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'relative',
    'display': 'table-cell',
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'borderRadius': '0'
  },
  'tabbar__item > input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none'
  },
  'tabbar__button': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'position': 'relative',
    'display': 'inline-block',
    'textDecoration': 'none',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 49 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': '#999',
    'textShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'px', 'value': 1 }],
    'verticalAlign': 'top',
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 49 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderTop': [{ 'unit': 'string', 'value': 'none' }]
    }
  },
  'tabbar__icon': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'lineHeight': [{ 'unit': 'px', 'value': 26 }],
    'display': 'block !important',
    // stylelint-disable-line declaration-no-important
    'height': [{ 'unit': 'px', 'value': 28 }]
  },
  'tabbar__label': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400'
  },
  'tabbar__badgenotification': {
    // FIXME
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 5 }],
    'zIndex': '10',
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 16 }],
    'lineHeight': [{ 'unit': 'px', 'value': 16 }],
    'borderRadius': '8px'
  },
  'tabbar__icon + tabbar__label': {
    'fontSize': [{ 'unit': 'px', 'value': 10 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontWeight': '400'
  },
  'tabbar__label:first-child': {
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'lineHeight': [{ 'unit': 'px', 'value': 49 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  ':checked + tabbar__button': {
    'color': 'rgba(24, 103, 194, .81)',
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'borderTop': [{ 'unit': 'string', 'value': 'none' }]
  },
  'tabbar__button:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'tabbar__button:focus': {
    'zIndex': '1',
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'outline': '0'
  },
  'tabbar__content': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 49 }],
    'zIndex': '0'
  },
  // ~
  name: Icon Only Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <!-- Prerequisite=This example use ionicons(http://ionicons.com) to display icons. -->
    <div class="tabbar">
      <label class="tabbar__item">
        <input type="radio" name="tabbar-b" checked="checked">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-stop"></i>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-b">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-record"></i>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="tabbar-b">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-star"></i>
        </button>
      </label>

    </div>
  // ~
  name: Top Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar tabbar--top">
      <label class="tabbar__item">
        <input type="radio" name="top-tabbar-a" checked="checked">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-stop"></i>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="top-tabbar-a">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-record"></i>
        </button>
      </label>

      <label class="tabbar__item">
        <input type="radio" name="top-tabbar-a">
        <button class="tabbar__button">
          <i class="tabbar__icon ion-star"></i>
        </button>
      </label>
    </div>
  'tabbar--top': {
    'position': 'relative',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'string', 'value': 'auto' }],
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
      'backgroundSize': '100% 1px',
      'backgroundRepeat': 'no-repeat',
      'backgroundPosition': 'bottom',
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'tabbar--top__content': {
    'top': [{ 'unit': 'px', 'value': 49 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '0'
  },
  // ~
  name: Bordered Top Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar tabbar--top tabbar--top-border">
      <label class="tabbar__item tabbar--top-border__item">
        <input type="radio" name="top-tabbar-b" checked="checked">
        <button class="tabbar__button tabbar--top-border__button">
          Home
        </button>
      </label>

      <label class="tabbar__item tabbar--top-border__item">
        <input type="radio" name="top-tabbar-b">
        <button class="tabbar__button tabbar--top-border__button">
          Comments
        </button>
      </label>

      <label class="tabbar__item tabbar--top-border__item">
        <input type="radio" name="top-tabbar-b">
        <button class="tabbar__button tabbar--top-border__button">
          Activity
        </button>
      </label>
    </div>
  // ~
  name: Material Tabbar
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar tabbar--top tabbar--material">
      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a" checked="checked">
        <button class="tabbar__button tabbar--material__button">
          Music
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          Movies
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          Books
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          Games
        </button>
      </label>

    </div>
  'tabbar--top-border__button': {
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }]
  },
  ':checked + tabbar--top-border__button': {
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }]
  },
  'tabbar--material': {
    'backgroundColor': '#009688',
    'borderBottomWidth': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -4 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }]
  },
  'tabbar--material__button': {
    'backgroundColor': 'transparent',
    'color': 'rgba(255, 255, 255, .6)',
    // text-transform: uppercase;
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'fontWeight': '500',
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'tabbar--material__button:after': {
    'content': '''',
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 2 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'position': 'absolute',
    'marginTop': [{ 'unit': 'px', 'value': -2 }],
    'backgroundColor': '#ffffff'
  },
  ':checked + tabbar--material__button:after': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'WebkitTransition': 'width 0.2s ease-in-out',
    'transition': 'width 0.2s ease-in-out'
  },
  ':checked + tabbar--material__button': {
    'backgroundColor': 'transparent',
    'color': '#ffffff'
  },
  'tabbar--material__item:not([ripple]):active': {
    'backgroundColor': '#26a69a'
  },
  // ~
  name: Material Tabbar (Icon only)
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar tabbar--top tabbar--material">
      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a" checked="checked">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-phone"></i>
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-favorite"></i>
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-pin-account"></i>
        </button>
      </label>
    </div>
  'tabbar--material__icon': {
    'fontSize': [{ 'unit': 'px', 'value': 22 }, { 'unit': 'string', 'value': '!important' }],
    // stylelint-disable-line declaration-no-important
    'lineHeight': [{ 'unit': 'px', 'value': 36 }]
  },
  // ~
  name: Material Tabbar (Icon and Label)
  category: Tabbar
  elements: ons-tabbar ons-tab
  markup: |
    <div class="tabbar tabbar--top tabbar--material">
      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a" checked="checked">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-phone"></i>
          <div class="tabbar__label tabbar--material__label">Call</div>
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-favorite"></i>
          <div class="tabbar__label tabbar--material__label">Favorites</div>
        </button>
      </label>

      <label class="tabbar__item tabbar--material__item">
        <input type="radio" name="tabbar-material-a">
        <button class="tabbar__button tabbar--material__button">
          <i class="tabbar__icon tabbar--material__icon zmdi zmdi-delete"></i>
          <div class="tabbar__label tabbar--material__label">Delete</div>
        </button>
      </label>
    </div>
  'tabbar--material__label': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'tabbar--material__label:first-child': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'letterSpacing': [{ 'unit': 'em', 'value': 0.015 }],
    'fontWeight': '500',
    'fontSize': [{ 'unit': 'px', 'value': 14 }]
  },
  'tabbar--material__icon + tabbar--material__label': {
    'fontSize': [{ 'unit': 'px', 'value': 10 }]
  },
  // ~
  name: Toolbar Button
  category: Toolbar Button
  elements: ons-toolbar-button
  markup: |
    <!-- Prerequisite=This example use font-awesome(http://fortawesome.github.io/Font-Awesome/) to display icons. -->
    <button class="toolbar-button">
      <i class="fa fa-bell" style="font-size:17px"></i> Label
    </button>

    <button class="toolbar-button toolbar-button--outline">
      <i class="fa fa-bell" style="font-size:17px"></i> Label
    </button>
  'toolbar-button': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 10 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': 'rgba(38, 100, 171, .81)',
    'textShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'px', 'value': 1 }],
    'backgroundColor': 'rgba(0, 0, 0, 0)',
    'borderRadius': '2px',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'toolbar-button:active': {
    'backgroundColor': 'rgba(0, 0, 0, 0)',
    'WebkitTransition': 'none',
    'transition': 'none',
    'opacity': '0.2'
  },
  'toolbar-button:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'toolbar-button[disabled]': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'toolbar-button:focus': {
    'outline': '0',
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'toolbar-button:hover': {
    'WebkitTransition': 'none',
    'transition': 'none'
  },
  'toolbar-button--outline': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(38, 100, 171, .81)' }],
    'margin': [{ 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 8 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 8 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 6 }],
    'paddingRight': [{ 'unit': 'px', 'value': 6 }]
  },
  'toolbar-button--material': {
    'fontSize': [{ 'unit': 'px', 'value': 22 }],
    'color': '#ffffff',
    'display': 'inline-block',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '0',
    'verticalAlign': 'baseline',
    'verticalAlign': 'initial'
  },
  'toolbar-button--material:first-of-type': {
    'marginLeft': [{ 'unit': 'px', 'value': 4 }]
  },
  'toolbar-button--material:last-of-type': {
    'marginRight': [{ 'unit': 'px', 'value': 4 }]
  },
  'toolbar-button--material:active': {
    'opacity': '1',
    'backgroundColor': 'rgba(255, 255, 255, .15)'
  },
  'back-button': {
    'height': [{ 'unit': 'px', 'value': 44 }],
    'lineHeight': [{ 'unit': 'px', 'value': 44 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 8 }],
    'color': 'rgba(38, 100, 171, .81)',
    'backgroundColor': 'rgba(0, 0, 0, 0)'
  },
  'back-button:active': {
    'opacity': '0.2'
  },
  'back-button__label': {
    'display': 'inline-block',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'verticalAlign': 'top'
  },
  'back-button__icon': {
    'display': 'inline-block',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'verticalAlign': 'top',
    'marginRight': [{ 'unit': 'px', 'value': 6 }]
  },
  'back-button__icon:before': {
    'fontFamily': ''Ionicons'',
    'content': ''\f3cf'',
    'fontSize': [{ 'unit': 'px', 'value': 32 }]
  },
  'back-button--material': {
    'fontSize': [{ 'unit': 'px', 'value': 22 }],
    'color': '#ffffff',
    'display': 'inline-block',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '0',
    'verticalAlign': 'baseline',
    'verticalAlign': 'initial',
    'lineHeight': [{ 'unit': 'px', 'value': 56 }]
  },
  'back-button--material__label': {
    'display': 'none',
    'fontSize': [{ 'unit': 'px', 'value': 20 }]
  },
  'back-button--material__icon:before': {
    'fontFamily': ''Material-Design-Iconic-Font'',
    'content': ''\f2ea'',
    'fontSize': [{ 'unit': 'px', 'value': 26 }]
  },
  'back-button--material:active': {
    'opacity': '1',
    'backgroundColor': 'rgba(255, 255, 255, .15)'
  },
  ':root': {
    // background color active
    // indicator color
  },
  // ~
  name: Checkbox
  category: Checkbox
  elements: ons-input
  markup: |
    <label class="checkbox">
      <input type="checkbox" class="checkbox__input">
      <div class="checkbox__checkmark"></div>
      OFF
    </label>

    <label class="checkbox">
      <input type="checkbox" class="checkbox__input" checked="checked">
      <div class="checkbox__checkmark"></div>
      ON
    </label>

    <label class="checkbox">
      <input type="checkbox" class="checkbox__input" disabled>
      <div class="checkbox__checkmark"></div>
      Disabled
    </label>
  'checkbox': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 24 }]
  },
  'checkbox__checkmark': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 24 }],
    'width': [{ 'unit': 'px', 'value': 24 }],
    'pointerEvents': 'none'
  },
  'checkbox__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none'
  },
  'checkbox__input:checked': {
    'background': 'rgba(24, 103, 194, .81)'
  },
  'checkbox__checkmark:before': {
    'content': '''',
    'position': 'absolute',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'borderRadius': '16px',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  // checkmark's line
  'checkbox__checkmark:after': {
    'content': '''',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 6 }],
    'left': [{ 'unit': 'px', 'value': 5 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 6 }],
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#fff' }],
    'borderWidth': '2px',
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'borderRight': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '0',
    'WebkitTransform': 'rotate(-45deg)',
    'transform': 'rotate(-45deg)',
    'opacity': '0'
  },
  ':checked + checkbox__checkmark:before': {
    'background': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }]
  },
  ':checked + checkbox__checkmark:after': {
    'opacity': '1'
  },
  ':focus + checkbox__checkmark:before': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  ':disabled + checkbox__checkmark': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  ':disabled:active + checkbox__checkmark:before': {
    // FIXME
    'background': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: No border Checkbox
  category: Checkbox
  elements: ons-input
  markup: |
    <label class="checkbox--noborder">
      <input type="checkbox" class="checkbox__input checkbox--noborder__input">
      <div class="checkbox__checkmark checkbox--noborder__checkmark"></div>
      OFF
    </label>

    <label class="checkbox--noborder">
      <input type="checkbox" class="checkbox__input checkbox--noborder__input" checked="checked">
      <div class="checkbox__checkmark checkbox--noborder__checkmark"></div>
      ON
    </label>

    <label class="checkbox--noborder">
      <input type="checkbox" class="checkbox__input checkbox--noborder__input" disabled checked="checked">
      <div class="checkbox__checkmark checkbox--noborder__checkmark"></div>
      Disabled
    </label>
  'checkbox--noborder': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 24 }],
    'position': 'relative'
  },
  'checkbox--noborder__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none'
  },
  'checkbox--noborder__checkmark': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'checkbox--noborder__checkmark:before': {
    'content': '''',
    'position': 'absolute',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '16px',
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'checkbox--noborder__checkmark:after': {
    'content': '''',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 6 }],
    'left': [{ 'unit': 'px', 'value': 5 }],
    'opacity': '0',
    'width': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 6 }],
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'borderWidth': '2px',
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'borderRight': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '0',
    'WebkitTransform': 'rotate(-45deg)',
    'transform': 'rotate(-45deg)'
  },
  ':checked + checkbox--noborder__checkmark:before': {
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  ':checked + checkbox--noborder__checkmark:after': {
    'opacity': '1'
  },
  ':focus + checkbox--noborder__checkmark:before': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  ':disabled + checkbox--noborder__checkmark': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  ':disabled:active + checkbox--noborder__checkmark:before': {
    'background': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  // ~
  name: Material Checkbox
  category: Checkbox
  elements: ons-input
  markup: |
    <label class="checkbox checkbox--material">
      <input type="checkbox" class="checkbox__input checkbox--material__input">
      <div class="checkbox__checkmark checkbox--material__checkmark"></div>
      OFF
    </label>
    <label class="checkbox checkbox--material">
      <input type="checkbox" class="checkbox__input checkbox--material__input" checked="checked">
      <div class="checkbox__checkmark checkbox--material__checkmark"></div>
      ON
    </label>
    <label class="checkbox checkbox--material">
      <input type="checkbox" class="checkbox__input checkbox--material__input" checked="checked" disabled>
      <div class="checkbox__checkmark checkbox--material__checkmark"></div>
      ON
    </label>
    <label class="checkbox checkbox--material">
      <input type="checkbox" class="checkbox__input checkbox--material__input" disabled>
      <div class="checkbox__checkmark checkbox--material__checkmark"></div>
      Disabled
    </label>
  'checkbox--material': {
    'lineHeight': [{ 'unit': 'px', 'value': 18 }],
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'checkbox--material__checkmark': {
    'width': [{ 'unit': 'px', 'value': 18 }],
    'height': [{ 'unit': 'px', 'value': 18 }]
  },
  'checkbox--material__checkmark:before': {
    'borderRadius': '2px',
    'height': [{ 'unit': 'px', 'value': 18 }],
    'width': [{ 'unit': 'px', 'value': 18 }],
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#717171' }]
  },
  ':checked + checkbox--material__checkmark:before': {
    'backgroundColor': '#009688',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'checkbox--material__checkmark:after': {
    'borderColor': '#ffffff',
    'WebkitTransition': '-webkit-transform 0.3s ease',
    'transition': '-webkit-transform 0.3s ease',
    'transition': 'transform 0.3s ease',
    'transition': 'transform 0.3s ease, -webkit-transform 0.3s ease',
    'width': [{ 'unit': 'px', 'value': 10 }],
    'height': [{ 'unit': 'px', 'value': 5 }],
    'top': [{ 'unit': 'px', 'value': 4 }],
    'left': [{ 'unit': 'px', 'value': 3 }],
    'WebkitTransform': 'scale(0) rotate(-45deg)',
    'transform': 'scale(0) rotate(-45deg)'
  },
  ':checked + checkbox--material__checkmark:after': {
    'top': [{ 'unit': 'px', 'value': 4 }],
    'left': [{ 'unit': 'px', 'value': 3 }],
    'WebkitTransform': 'scale(1) rotate(-45deg)',
    'transform': 'scale(1) rotate(-45deg)'
  },
  ':disabled + checkbox--material__checkmark': {
    'opacity': '1'
  },
  ':disabled + checkbox--material__checkmark:before': {
    'borderColor': '#afafaf'
  },
  ':disabled:checked + checkbox--material__checkmark:before': {
    'backgroundColor': '#afafaf'
  },
  ':disabled:checked + checkbox--material__checkmark:after': {
    'borderColor': '#ffffff'
  },
  // ~
  name: Radio Button
  category: Radio Button
  elements: ons-input
  markup: |
    <label class="radio-button">
      <input type="radio" class="radio-button__input" name="r" checked="checked">
      <div class="radio-button__checkmark"></div>
      Label
    </label>

    <label class="radio-button">
      <input type="radio" class="radio-button__input" name="r">
      <div class="radio-button__checkmark"></div>
      Label
    </label>

    <label class="radio-button">
      <input type="radio" class="radio-button__input" name="r">
      <div class="radio-button__checkmark"></div>
      Label
    </label>
  'radio-button__input': {
    'position': 'absolute',
    'overflow': 'hidden',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }],
    'opacity': '0.001',
    'zIndex': '1',
    'verticalAlign': 'top',
    'outline': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none'
  },
  'radio-button__input:active': {
    'outline': '0',
    'WebkitTapHighlightColor': 'rgba(0, 0, 0, 0)'
  },
  'radio-button__input:focus': {
    'outline': '0',
    'WebkitTapHighlightColor': 'rgba(0, 0, 0, 0)'
  },
  'radio-button': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'position': 'relative',
    'lineHeight': [{ 'unit': 'px', 'value': 24 }],
    'textAlign': 'left'
  },
  'radio-button__checkmark:before': {
    'content': '''',
    'position': 'absolute',
    'borderRadius': '100%',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '16px',
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'radio-button__checkmark': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'top',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'relative',
    'position': 'relative',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': 'transparent',
    'pointerEvents': 'none'
  },
  'radio-button__checkmark:after': {
    'content': '''',
    'position': 'absolute',
    'borderRadius': '100%',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'WebkitTransform': 'translate(-50%, -50%)',
    'transform': 'translate(-50%, -50%)',
    'top': [{ 'unit': 'px', 'value': 6 }],
    'left': [{ 'unit': 'px', 'value': 5 }],
    'opacity': '0',
    'width': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 6 }],
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(24, 103, 194, .81)' }],
    'borderWidth': '2px',
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'borderRight': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '0',
    'WebkitTransform': 'rotate(-45deg)',
    'transform': 'rotate(-45deg)'
  },
  ':checked + radio-button__checkmark': {
    'background': 'rgba(0, 0, 0, 0)'
  },
  ':checked + radio-button__checkmark:after': {
    'opacity': '1'
  },
  ':checked + radio-button__checkmark:before': {
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  ':disabled + radio-button__checkmark': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // ~
  name: Material Radio Button
  category: Radio Button
  elements: ons-input
  markup: |
    <label class="radio-button radio-button--material">
      <input type="radio" class="radio-button__input radio-button--material__input" name="r" checked="checked">
      <div class="radio-button__checkmark radio-button--material__checkmark"></div>
      Label
    </label>
    <label class="radio-button radio-button--material">
      <input type="radio" class="radio-button__input radio-button--material__input" name="r">
      <div class="radio-button__checkmark radio-button--material__checkmark"></div>
      Label
    </label>
    <label class="radio-button radio-button--material">
      <input type="radio" class="radio-button__input radio-button--material__input" name="s" disabled checked>
      <div class="radio-button__checkmark radio-button--material__checkmark"></div>
      Label
    </label>
    <label class="radio-button radio-button--material">
      <input type="radio" class="radio-button__input radio-button--material__input" name="s" disabled>
      <div class="radio-button__checkmark radio-button--material__checkmark"></div>
      Label
    </label>
  'radio-button--material': {
    'lineHeight': [{ 'unit': 'px', 'value': 20 }, { 'unit': 'string', 'value': '+' }, { 'unit': 'px', 'value': 2 }],
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'radio-button--material__checkmark': {
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'overflow': 'visible'
  },
  'radio-button--material__checkmark:before': {
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#717171' }],
    'boxSizing': 'border-box',
    'borderRadius': '50%',
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'WebkitTransition': 'border 0.2s ease',
    'transition': 'border 0.2s ease'
  },
  'radio-button--material__checkmark:after': {
    'WebkitTransition': 'background 0.2s ease, -webkit-transform 0.2s ease',
    'transition': 'background 0.2s ease, -webkit-transform 0.2s ease',
    'transition': 'background 0.2s ease, transform 0.2s ease',
    'transition': 'background 0.2s ease, transform 0.2s ease, -webkit-transform 0.2s ease',
    'top': [{ 'unit': 'px', 'value': 5 }],
    'left': [{ 'unit': 'px', 'value': 5 }],
    'width': [{ 'unit': 'px', 'value': 10 }],
    'height': [{ 'unit': 'px', 'value': 10 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderRadius': '50%',
    'WebkitTransform': 'scale(0)',
    'transform': 'scale(0)'
  },
  ':checked + radio-button--material__checkmark:before': {
    'background': 'transparent',
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#009688' }]
  },
  'radio-button--material__input + radio-button__checkmark:after': {
    'background': '#717171',
    'opacity': '1',
    'WebkitTransform': 'scale(0)',
    'transform': 'scale(0)'
  },
  ':checked + radio-button--material__checkmark:after': {
    'opacity': '1',
    'background': '#009688',
    'WebkitTransform': 'scale(1)',
    'transform': 'scale(1)'
  },
  ':disabled + radio-button--material__checkmark': {
    'opacity': '1'
  },
  ':disabled + radio-button--material__checkmark:after': {
    'backgroundColor': '#afafaf',
    'borderColor': '#afafaf'
  },
  ':disabled + radio-button--material__checkmark:before': {
    'borderColor': '#afafaf'
  },
  // ~
  name: List
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">Dog</div>
      </li>
      <li class="list-item">
        <div class="list-item__center">Cat</div>
      </li>
      <li class="list-item">
        <div class="list-item__center">Hamster</div>
      </li>
    </ul>
  'list': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'listStyleType': 'none',
    'textAlign': 'left',
    'display': 'block',
    'WebkitOverflowScrolling': 'touch',
    'overflow': 'hidden',
    'backgroundImage': '-webkit-linear-gradient(#ccc, #ccc),
    -webkit-linear-gradient(#ccc, #ccc)',
    'backgroundImage': 'linear-gradient(#ccc, #ccc),
    linear-gradient(#ccc, #ccc)',
    'backgroundSize': '100% 1px, 100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom, top',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': '#fff',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%),
      -webkit-linear-gradient(top, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%),
      linear-gradient(180deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'list-item': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'position': 'relative',
    'listStyle': 'none',
    'boxSizing': 'border-box',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitBoxOrient': 'horizontal',
    'WebkitBoxDirection': 'normal',
    'MsFlexDirection': 'row',
    'flexDirection': 'row',
    'WebkitBoxPack': 'start',
    'MsFlexPack': 'start',
    'justifyContent': 'flex-start',
    'WebkitBoxAlign': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 14 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': 0 }],
    'color': '#1f1f21',
    'WebkitTransition': 'background-color 0.2s linear',
    'transition': 'background-color 0.2s linear'
  },
  'list-item__left': {
    'boxSizing': 'border-box',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'padding': [{ 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }],
    'WebkitBoxOrdinalGroup': '1',
    'MsFlexOrder': '0',
    'order': '0',
    'WebkitBoxAlign': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'MsFlexItemAlign': 'stretch',
    'alignSelf': 'stretch',
    'lineHeight': [{ 'unit': 'em', 'value': 1.2 }],
    'minHeight': [{ 'unit': 'px', 'value': 44 }]
  },
  'list-item__left:empty': {
    'width': [{ 'unit': 'px', 'value': 0 }],
    'minWidth': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'list-item__center': {
    'boxSizing': 'border-box',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitBoxFlex': '1',
    'MsFlexPositive': '1',
    'flexGrow': '1',
    'MsFlexWrap': 'wrap',
    'flexWrap': 'wrap',
    'WebkitBoxOrient': 'horizontal',
    'WebkitBoxDirection': 'normal',
    'MsFlexDirection': 'row',
    'flexDirection': 'row',
    'WebkitBoxOrdinalGroup': '2',
    'MsFlexOrder': '1',
    'order': '1',
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }],
    'WebkitBoxAlign': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'MsFlexItemAlign': 'stretch',
    'alignSelf': 'stretch',
    'marginLeft': [{ 'unit': 'px', 'value': 0 }],
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 100%)',
    'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 100%)',
    'padding': [{ 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }],
    'lineHeight': [{ 'unit': 'em', 'value': 1.2 }],
    'minHeight': [{ 'unit': 'px', 'value': 44 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'list-item__right': {
    'boxSizing': 'border-box',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'padding': [{ 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }],
    'WebkitBoxOrdinalGroup': '3',
    'MsFlexOrder': '2',
    'order': '2',
    'WebkitBoxAlign': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'MsFlexItemAlign': 'stretch',
    'alignSelf': 'stretch',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 100%)',
    'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 100%)',
    'lineHeight': [{ 'unit': 'em', 'value': 1.2 }],
    'minHeight': [{ 'unit': 'px', 'value': 44 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'list-header': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'listStyle': 'none',
    'textAlign': 'left',
    'display': 'block',
    'boxSizing': 'border-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 15 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'fontWeight': '500',
    'color': '#1f1f21',
    'minHeight': [{ 'unit': 'px', 'value': 24 }],
    'lineHeight': [{ 'unit': 'px', 'value': 25 }],
    // text-transform: uppercase;
    'position': 'relative',
    'backgroundColor': '#eee',
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'top',
    'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 100%)',
    'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(top, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(180deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  // ~
  name: Noborder List
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list list--noborder">
      <li class="list-item list-item--noborder">
        <div class="list-item__center">Item</div>
      </li>
      <li class="list-item list-item--noborder">
        <div class="list-item__center">Item</div>
      </li>
    </ul>
  'list--noborder': {
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  // ~
  name: Category List Header
  category: List
  elements: ons-list ons-list-header ons-list-item
  markup: |
    <ul class="list">
      <li class="list-header">
        Header
      </li>
      <li class="list-item">
        <div class="list-item__center">Item</div>
      </li>
      <li class="list-item">
        <div class="list-item__center">Item</div>
      </li>
      <li class="list-item">
        <div class="list-item__center">Item</div>
      </li>
    </ul>
  // ~
  name: Tappable List
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item list-item--tappable">
        <div class="list-item__center">Tappable Item</div>
      </li>
      <li class="list-item list-item--tappable">
        <div class="list-item__center">Tappable Item</div>
      </li>
      <li class="list-item list-item--tappable">
        <div class="list-item__center">Tappable Item</div>
      </li>
    </ul>
  'list-item--tappable:active': {
    'WebkitTransition': 'none',
    'transition': 'none',
    'backgroundColor': '#d9d9d9'
  },
  // ~
  name: Switch in List Item
  category: List
  elements: ons-list ons-list-item ons-switch
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">
          Label
        </div>
        <div class="list-item__right">
          <label class="switch">
            <input type="checkbox" class="switch__input" checked>
            <div class="switch__toggle">
              <div class="switch__handle"></div>
            </div>
          </label>
        </div>
      </li>
      <li class="list-item">
        <div class="list-item__center">
          Label
        </div>
        <div class="list-item__right">
          <label class="switch">
            <input type="checkbox" class="switch__input">
            <div class="switch__toggle">
              <div class="switch__handle"></div>
            </div>
          </label>
        </div>
      </li>
    </ul>
  // ~
  name: List Item with Chevron
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item list-item--chevron">
        <div class="list-item__center">Item A</div>
      </li>
      <li class="list-item list-item--chevron">
        <div class="list-item__center">Item B</div>
      </li>
    </ul>
  'list-item--chevron': {
    'overflow': 'hidden'
  },
  'list-item--chevron:before': {
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitBoxAlign': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'color': '#ccc',
    'WebkitBoxOrdinalGroup': '4',
    'MsFlexOrder': '3',
    'order': '3',
    'MsFlexItemAlign': 'stretch',
    'alignSelf': 'stretch',
    'fontSize': [{ 'unit': 'px', 'value': 28 }],
    'fontFamily': ''FontAwesome'',
    'fontStyle': 'normal',
    'fontWeight': '200',
    'content': ''\f105'',
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'textAlign': 'right',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 100%)',
    'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 100%)',
    'width': [{ 'unit': 'px', 'value': 10 }],
    'paddingRight': [{ 'unit': 'px', 'value': 8 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  // ~
  name: Inset List
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list list--inset">
      <li class="list-item list--inset__item list-item--chevron list-item--tappable">
        <div class="list-item__center">List Item with Chevron</div>
      </li>
      <li class="list-item list--inset__item list-item--chevron list-item--tappable">
        <div class="list-item__center">List Item with Chevron</div>
      </li>
    </ul>
  'list--inset': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderRadius': '4px',
    'backgroundImage': 'none'
  },
  // ~
  name: Radio Button in List Item
  category: List
  elements: ons-list ons-list-item ons-input
  markup: |
    <ul class="list">
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="radio-button">
            <input type="radio" id="r1" class="radio-button__input" name="r" checked="checked">
            <div class="radio-button__checkmark"></div>
          </label>
        </div>
        <label for="r1" class="list-item__center">
          Radio Button
        </label>
      </li>
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="radio-button">
            <input type="radio" id="r2" class="radio-button__input" name="r">
            <div class="radio-button__checkmark"></div>
          </label>
        </div>
        <label for="r2" class="list-item__center">
          Radio Button
        </label>
      </li>
    </ul>
  // ~
  name: Checkbox in List Item
  category: List
  elements: ons-list ons-list-item ons-input
  markup: |
    <ul class="list">
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="checkbox">
            <input type="checkbox" id="checkbox1" class="checkbox__input" name="c" checked="checked">
            <div class="checkbox__checkmark"></div>
          </label>
        </div>
        <label for="checkbox1" class="list-item__center">
          Checkbox
        </label>
      </li>
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="checkbox">
            <input type="checkbox" id="checkbox2" class="checkbox__input" name="c">
            <div class="checkbox__checkmark"></div>
          </label>
        </div>
        <label for="checkbox2" class="list-item__center">
          Checkbox
        </label>
      </li>
    </ul>
  // ~
  name: No border Checkbox in List Item
  category: List
  elements: ons-list ons-list-item ons-input
  markup: |
    <ul class="list">
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="checkbox checkbox--noborder">
            <input id="s1" type="checkbox" class="checkbox__input checkbox--noborder__input">
            <div class="checkbox__checkmark checkbox--noborder checkbox--noborder__checkmark"></div>
          </label>
        </div>
        <label for="s1" class="list-item__center">
          Checkbox
        </label>
      </li>
      <li class="list-item list-item--tappable">
        <div class="list-item__left">
          <label class="checkbox checkbox--noborder">
            <input id="s2" type="checkbox" class="checkbox__input checkbox--noborder__input" checked>
            <div class="checkbox__checkmark checkbox--noborder checkbox--noborder__checkmark"></div>
          </label>
        </div>
        <label for="s2" class="list-item__center">
          Checkbox
        </label>
      </li>
    </ul>
  // ~
  name: Text Input in List Item
  category: List
  elements: ons-list ons-list-item ons-input
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">
          <input type="text" class="text-input" placeholder="Name">
        </div>
      </li>
      <li class="list-item">
        <div class="list-item__center">
          <input type="text" class="text-input" placeholder="Email">
        </div>
      </li>
    </ul>
  // ~
  name: Textarea in List Item
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">
          <textarea class="textarea textarea--transparent" placeholder="Text message"></textarea>
        </div>
      </li>
    </ul>
  // ~
  name: Right Label in List Item
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">
          Title
        </div>
        <div class="list-item__right">
          <div class="list-item__label">Label</div>
        </div>
      </li>
    </ul>
  'list-item__label': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }],
    'opacity': '0.6'
  },
  // ~
  name: List Item with Subtitle
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">
          <div class="list-item__title">
            Title
          </div>
          <div class="list-item__subtitle">
            Subtitle
          </div>
        </div>
      </li>
    </ul>
  'list-item__title': {
    'MsFlexPreferredSize': '100%',
    'flexBasis': '100%',
    'MsFlexItemAlign': 'end',
    'alignSelf': 'flex-end',
    'WebkitBoxOrdinalGroup': '1',
    'MsFlexOrder': '0',
    'order': '0'
  },
  'list-item__subtitle': {
    'opacity': '0.75',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'WebkitBoxOrdinalGroup': '2',
    'MsFlexOrder': '1',
    'order': '1',
    'MsFlexPreferredSize': '100%',
    'flexBasis': '100%',
    'MsFlexItemAlign': 'start',
    'alignSelf': 'flex-start'
  },
  // ~
  name: List Item with Thumbnail
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__left">
          <img class="list-item__thumbnail" src="http://placekitten.com/g/40/40" alt="Cute kitten">
        </div>

        <div class="list-item__center">
          <div class="list-item__title">Lily</div>
          <div class="list-item__subtitle">Very friendly cat</div>
        </div>
      </li>

      <li class="list-item">
        <div class="list-item__left">
          <img class="list-item__thumbnail" src="http://placekitten.com/g/40/40" alt="Cute kitten">
        </div>

        <div class="list-item__center">
          <div class="list-item__title">Molly</div>
          <div class="list-item__subtitle">Loves tuna!</div>
        </div>
      </li>
    </ul>
  'list-item__thumbnail': {
    'width': [{ 'unit': 'px', 'value': 40 }],
    'height': [{ 'unit': 'px', 'value': 40 }],
    'borderRadius': '6px',
    'display': 'block',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  // ~
  name: List Item with Icon
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list">
      <li class="list-item">
        <div class="list-item__left">
          <img class="list-item__thumbnail" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAIAAAADnC86AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wwJCB8v/9zErgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAvSURBVFjD7c0BDQAACAMgtX+KJzWGm4MCdJK6MHVELBaLxWKxWCwWi8VisVj8MV7qBgI2A8rYpgAAAABJRU5ErkJggg==">
        </div>

        <div class="list-item__center">
          <div class="list-item__title">Alice</div>
          <div class="list-item__subtitle">Description</div>
        </div>

        <div class="list-item__right">
          <i class="ion-ios-information list-item__icon"></i>
        </div>
      </li>

      <li class="list-item">
        <div class="list-item__left">
          <img class="list-item__thumbnail" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAIAAAADnC86AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wwJCB8v/9zErgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAvSURBVFjD7c0BDQAACAMgtX+KJzWGm4MCdJK6MHVELBaLxWKxWCwWi8VisVj8MV7qBgI2A8rYpgAAAABJRU5ErkJggg==">
        </div>

        <div class="list-item__center">
          <div class="list-item__title">Bob</div>
          <div class="list-item__subtitle">Description</div>
        </div>

        <div class="list-item__right">
          <i class="ion-ios-information list-item__icon"></i>
        </div>
      </li>
    </ul>
  'list-item__icon': {
    'fontSize': [{ 'unit': 'px', 'value': 22 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }]
  },
  // ~
  name: Material List
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list list--material">
      <li class="list-item list-item--material">
        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Orange</div>
          <div class="list-item__subtitle list-item--material__subtitle">Sweet fruit that grows on trees.</div>
        </div>
      </li>
      <li class="list-item list-item--material">
        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Pear</div>
          <div class="list-item__subtitle list-item--material__subtitle">Funny-shaped fruit.</div>
        </div>
      </li>
    </ul>
  'list--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'backgroundImage': 'none'
  },
  'list-item--material': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }]
  },
  'list-item--material__subtitle': {
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'list-item--material:first-child': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'list-item--material__left': {
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 0 }],
    'minWidth': [{ 'unit': 'px', 'value': 56 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'minHeight': [{ 'unit': 'px', 'value': 48 }]
  },
  'list-item--material__left:empty': {
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 0 }],
    'borderColor': '#eee',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 100%)',
    'backgroundImage': 'linear-gradient(0deg, #eee, #eee 100%)',
    'minHeight': [{ 'unit': 'px', 'value': 48 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #eee, #eee 50%, transparent 50%)'
    }
  },
  'list-item--material__center': {
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 0 }],
    'borderColor': '#eee',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 100%)',
    'backgroundImage': 'linear-gradient(0deg, #eee, #eee 100%)',
    'minHeight': [{ 'unit': 'px', 'value': 48 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #eee, #eee 50%, transparent 50%)'
    }
  },
  'list-item--material__right': {
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 0 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'borderColor': '#eee',
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 100%)',
    'backgroundImage': 'linear-gradient(0deg, #eee, #eee 100%)',
    'minHeight': [{ 'unit': 'px', 'value': 48 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #eee, #eee 50%, transparent 50%)'
    }
  },
  'list-item--materiallist-item--longdivider': {
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }],
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 100%)',
    'backgroundImage': 'linear-gradient(0deg, #eee, #eee 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #eee, #eee 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #eee, #eee 50%, transparent 50%)'
    }
  },
  // ~
  name: Material List with Header
  category: List
  elements: ons-list ons-list-item
  markup: |
    <ul class="list list--material">
      <li class="list-header list-header--material">
        Fruits
      </li>
      <li class="list-item list-item--material">
        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Orange</div>
          <div class="list-item__subtitle list-item--material__subtitle">Sweet fruit that grows on trees.</div>
        </div>
      </li>
    </ul>
  'list-header--material': {
    'background': '#fff',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textTransform': 'none',
    'margin': [{ 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'color': '#757575',
    'fontWeight': '500',
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }]
  },
  'list-header--material:not(:first-of-type)': {
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'top',
    'backgroundImage': '-webkit-linear-gradient(top, #eee, #eee 100%)',
    'backgroundImage': 'linear-gradient(180deg, #eee, #eee 100%)',
    'paddingTop': [{ 'unit': 'px', 'value': 16 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(top, #eee, #eee 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(180deg, #eee, #eee 50%, transparent 50%)'
    }
  },
  // ~
  name: Material List with Checkboxes
  category: List
  elements: ons-list ons-list-item ons-input
  markup: |
    <ul class="list list--material">
      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <label class="checkbox checkbox--material">
            <input type="checkbox" id="checkbox3" class="checkbox__input checkbox--material__input">
            <div class="checkbox__checkmark checkbox--material__checkmark"></div>
          </label>
        </div>

        <label for="checkbox3" class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Notifications</div>
          <div class="list-item__subtitle list-item--material__subtitle">Allow notifications</div>
        </label>
      </li>

      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <label class="checkbox checkbox--material">
            <input type="checkbox" id="checkbox4" class="checkbox__input checkbox--material__input" checked="checked">
            <div class="checkbox__checkmark checkbox--material__checkmark"></div>
          </label>
        </div>

        <label for="checkbox4" class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Sound</div>
          <div class="list-item__subtitle list-item--material__subtitle">Hangouts message</div>
        </label>
      </li>

    </ul>
  // ~
  name: Material List with Thumbnails
  category: List
  markup: |
    <ul class="list list--material">
      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <img class="list-item__thumbnail list-item--material__thumbnail" src="http://placekitten.com/g/42/41" alt="Cute kitten">
        </div>

        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Lily</div>
          <div class="list-item__subtitle list-item--material__subtitle">Very friendly cat</div>
        </div>
      </li>

      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <img class="list-item__thumbnail list-item--material__thumbnail" src="http://placekitten.com/g/40/40" alt="Cute kitten">
        </div>

        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Molly</div>
          <div class="list-item__subtitle list-item--material__subtitle">Loves tuna!</div>
        </div>
      </li>

    </ul>
  'list-item--material__thumbnail': {
    'width': [{ 'unit': 'px', 'value': 40 }],
    'height': [{ 'unit': 'px', 'value': 40 }],
    'borderRadius': '100%'
  },
  // ~
  name: Material List with Icons
  category: List
  markup: |
    <ul class="list list--material">
      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <img class="list-item__thumbnail list-item--material__thumbnail" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAIAAAADnC86AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wwJCB8v/9zErgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAvSURBVFjD7c0BDQAACAMgtX+KJzWGm4MCdJK6MHVELBaLxWKxWCwWi8VisVj8MV7qBgI2A8rYpgAAAABJRU5ErkJggg==">
        </div>

        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Alice</div>
          <div class="list-item__subtitle list-item--material__subtitle">Description</div>
        </div>

        <div class="list-item__right list-item--material__right">
          <i style="color:#4db6ac" class="list-item__icon list-item--material__icon zmdi zmdi-comment"></i>
        </div>
      </li>

      <li class="list-item list-item--material">
        <div class="list-item__left list-item--material__left">
          <img class="list-item__thumbnail list-item--material__thumbnail" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAIAAAADnC86AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wwJCB8v/9zErgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAvSURBVFjD7c0BDQAACAMgtX+KJzWGm4MCdJK6MHVELBaLxWKxWCwWi8VisVj8MV7qBgI2A8rYpgAAAABJRU5ErkJggg==">
        </div>

        <div class="list-item__center list-item--material__center">
          <div class="list-item__title list-item--material__title">Bob</div>
          <div class="list-item__subtitle list-item--material__subtitle">Description</div>
        </div>

        <div class="list-item__right list-item--material__right">
          <i style="color:#4db6ac" class="list-item__icon list-item--material__icon zmdi zmdi-comment"></i>
        </div>
      </li>
    </ul>
  'list-item--material__icon': {
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }]
  },
  // ~
  name: List item without divider
  category: List
  markup: |
    <ul class="list">
      <li class="list-item list-item--nodivider">
        <div class="list-item__center list-item--nodivider__center">Item</div>
      </li>
      <li class="list-item list-item--nodivider">
        <div class="list-item__center list-item--nodivider__center">Item</div>
      </li>
    </ul>
  'list-item--nodivider__center': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  'list-item--nodivider__right': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  'list-item--nodivider:before': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  // ~
  name: List item with long divider
  category: List
  markup: |
    <ul class="list">
      <li class="list-item list-item--longdivider">
        <div class="list-item__center list-item--longdivider__center">Item</div>
      </li>
      <li class="list-item list-item--longdivider">
        <div class="list-item__center list-item--longdivider__center">Item</div>
      </li>
    </ul>
  'list-item--longdivider': {
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderBottom': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 100%)',
    'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, #ccc, #ccc 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, #ccc, #ccc 50%, transparent 50%)'
    }
  },
  'list-item--longdivider:last-of-type': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  'list-item--longdivider__center': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  'list-item--longdivider__right': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  'list-item--longdividerlist-item--chevron:before': {
    // FIXME
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundImage': 'none'
  },
  // ~
  name: List title
  category: List
  markup: |
    <div class="list-title">LIST TITLE</div>
    <ul class="list">
      <li class="list-item">
        <div class="list-item__center">Item</div>
      </li>
      <li class="list-item">
        <div class="list-item__center">Item</div>
      </li>
    </ul>
  'list-title': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': 'block',
    'color': '#6d6d72',
    'textAlign': 'left',
    'boxSizing': 'border-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 24 }]
  },
  // ~
  name: Material List Title
  category: List
  markup: |
    <h3 class="list-title list-title--material">LIST TITLE</h3>
    <ul class="list list--material">
      <li class="list-item list-item--material">
        <div class="list-item__center list-item--material__center">Item</div>
      </li>
      <li class="list-item list-item--material">
        <div class="list-item__center list-item--material__center">Item</div>
      </li>
    </ul>
  'list-title--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': '#757575',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 16 }],
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 24 }]
  },
  // ~
  name: Search Input
  category: Search Input
  markup: |
    <input type="search" value="" placeholder="Search" class="search-input" style="width: 280px;">
  'search-input': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'WebkitAppearance': 'textfield',
    'MozAppearance': 'textfield',
    'appearance': 'textfield',
    'boxSizing': 'border-box',
    'height': [{ 'unit': 'px', 'value': 28 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'backgroundColor': 'rgba(3, 3, 3, .09)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'color': '#1f1f21',
    'lineHeight': [{ 'unit': 'px', 'value': 1.3 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 28 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'borderRadius': '5.5px',
    'backgroundImage': 'url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTNweCIgaGVpZ2h0PSIxNHB4IiB2aWV3Qm94PSIwIDAgMTMgMTQiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQyICgzNjc4MSkgLSBodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2ggLS0+CiAgICA8dGl0bGU+aW9zLXNlYXJjaC1pbnB1dC1pY29uPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9ImNvbXBvbmVudHMiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJpb3Mtc2VhcmNoLWlucHV0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNDguMDAwMDAwLCAtNDMuMDAwMDAwKSIgZmlsbD0iIzdBNzk3QiI+CiAgICAgICAgICAgIDxnIGlkPSJHcm91cCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNDAuMDAwMDAwLCAzNi4wMDAwMDApIj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNi45OTcyNDgyLDE1LjUwNDE0NjYgQzE3LjA3NzM2NTcsMTUuNTQwNTkzOCAxNy4xNTIyNzMxLDE1LjU5MTYxMjkgMTcuMjE3NzUxNiwxNS42NTcwOTE0IEwyMC42NDk5OTEsMTkuMDg5MzMwOCBDMjAuOTQ0ODQ0OSwxOS4zODQxODQ3IDIwLjk0ODQ3NjQsMTkuODU4NjA2IDIwLjY1MzU0MTIsMjAuMTUzNTQxMiBDMjAuMzYwNjQ4LDIwLjQ0NjQzNDQgMTkuODgxMjcxNiwyMC40NDE5MzE3IDE5LjU4OTMzMDgsMjAuMTQ5OTkxIEwxNi4xNTcwOTE0LDE2LjcxNzc1MTYgQzE2LjA5MTM3LDE2LjY1MjAzMDEgMTYuMDQwMTE3MSwxNi41NzczODc0IDE2LjAwMzQxNDEsMTYuNDk3Nzk5NSBDMTUuMTY3MTY5NCwxNy4xMjcwNDExIDE0LjEyNzEzOTMsMTcuNSAxMywxNy41IEMxMC4yMzg1NzYzLDE3LjUgOCwxNS4yNjE0MjM3IDgsMTIuNSBDOCw5LjczODU3NjI1IDEwLjIzODU3NjMsNy41IDEzLDcuNSBDMTUuNzYxNDIzNyw3LjUgMTgsOS43Mzg1NzYyNSAxOCwxMi41IEMxOCwxMy42Mjc0Njg1IDE3LjYyNjgyMzIsMTQuNjY3Nzc2OCAxNi45OTcyNDgyLDE1LjUwNDE0NjYgWiBNMTMsMTYuNSBDMTUuMjA5MTM5LDE2LjUgMTcsMTQuNzA5MTM5IDE3LDEyLjUgQzE3LDEwLjI5MDg2MSAxNS4yMDkxMzksOC41IDEzLDguNSBDMTAuNzkwODYxLDguNSA5LDEwLjI5MDg2MSA5LDEyLjUgQzksMTQuNzA5MTM5IDEwLjc5MDg2MSwxNi41IDEzLDE2LjUgWiIgaWQ9Imlvcy1zZWFyY2gtaW5wdXQtaWNvbiI+PC9wYXRoPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=')',
    'backgroundPosition': '8px center',
    'backgroundRepeat': 'no-repeat',
    'backgroundSize': '13px',
    'fontWeight': '400',
    'display': 'inline-block',
    'textIndent': '0'
  },
  'search-input::-webkit-search-cancel-button': {
    'WebkitAppearance': 'textfield',
    'appearance': 'textfield',
    'display': 'none'
  },
  'search-input::-webkit-search-decoration': {
    'display': 'none'
  },
  'search-input:focus': {
    'outline': 'none'
  },
  'search-input::-webkit-input-placeholder': {
    'color': '#7a797b',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textIndent': '0'
  },
  'search-input::-moz-placeholder': {
    'color': '#7a797b',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textIndent': '0'
  },
  'search-input:-ms-input-placeholder': {
    'color': '#7a797b',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textIndent': '0'
  },
  'search-input::placeholder': {
    'color': '#7a797b',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textIndent': '0'
  },
  'search-input:placeholder-shown': {
  },
  'search-input:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // ~
  name: Material Search Input
  category: Search Input
  markup: |
    <input type="search" value="" placeholder="Search" class="search-input search-input--material" style="width: 280px;">
  'search-input--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'borderRadius': '2px',
    'height': [{ 'unit': 'px', 'value': 48 }],
    'backgroundColor': '#fafafa',
    'backgroundImage': 'url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMThweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMTggMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQyICgzNjc4MSkgLSBodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2ggLS0+CiAgICA8dGl0bGU+YW5kcm9pZC1zZWFyY2gtaW5wdXQtaWNvbjwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSJjb21wb25lbnRzIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBvcGFjaXR5PSIwLjU0Ij4KICAgICAgICA8ZyBpZD0ibWF0ZXJpYWwtc2VhcmNoIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTIuMDAwMDAwLCAtNDEuMDAwMDAwKSIgZmlsbD0iIzAwMDAwMCI+CiAgICAgICAgICAgIDxnIGlkPSJzZWFyY2giIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMzLjAwMDAwMCwgMjYuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8ZyBpZD0iTWF0ZXJpYWwvSWNvbnMtYmxhY2svc2VhcmNoIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNi4wMDAwMDAsIDEyLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNS41MDIsOS40OTEgTDE0LjcwOCw5LjQ5MSBMMTQuNDMyLDkuNzY1IEMxNS40MDcsMTAuOTAyIDE2LDEyLjM3NiAxNiwxMy45OTEgQzE2LDE3LjU4MSAxMy4wOSwyMC40OTEgOS41LDIwLjQ5MSBDNS45MSwyMC40OTEgMywxNy41ODEgMywxMy45OTEgQzMsMTAuNDAxIDUuOTEsNy40OTEgOS41LDcuNDkxIEMxMS4xMTUsNy40OTEgMTIuNTg4LDguMDgzIDEzLjcyNSw5LjA1NyBMMTQuMDAxLDguNzgzIEwxNC4wMDEsNy45OTEgTDE4Ljk5OSwzIEwyMC40OSw0LjQ5MSBMMTUuNTAyLDkuNDkxIEwxNS41MDIsOS40OTEgWiBNOS41LDkuNDkxIEM3LjAxNCw5LjQ5MSA1LDExLjUwNSA1LDEzLjk5MSBDNSwxNi40NzYgNy4wMTQsMTguNDkxIDkuNSwxOC40OTEgQzExLjk4NSwxOC40OTEgMTQsMTYuNDc2IDE0LDEzLjk5MSBDMTQsMTEuNTA1IDExLjk4NSw5LjQ5MSA5LjUsOS40OTEgTDkuNSw5LjQ5MSBaIiBpZD0iU2hhcGUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDExLjc0NTAwMCwgMTEuNzQ1NTAwKSBzY2FsZSgxLCAtMSkgdHJhbnNsYXRlKC0xMS43NDUwMDAsIC0xMS43NDU1MDApICI+PC9wYXRoPgogICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=')',
    'backgroundSize': '18px',
    'backgroundPosition': '18px center',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 64 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .24)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .24),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(255, 255, 255, .6)' }, { 'unit': 'string', 'value': 'rgba(255, 255, 255, .6)' }, { 'unit': 'string', 'value': 'inset' }]
  },
  // ~
  name: Text Input
  category: Text Input
  elements: ons-input
  markup: |
    <div><input type="text" class="text-input" placeholder="text" value=""></div>
    <div><input type="text" class="text-input" placeholder="text" value="" disabled></div>
  'text-input': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'color': '#1f1f21',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 31 }],
    'fontWeight': '400',
    'boxSizing': 'border-box'
  },
  'text-input::-ms-clear': {
    'display': 'none'
  },
  'text-input:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'text-input::-webkit-input-placeholder': {
    'color': '#999'
  },
  'text-input::-moz-placeholder': {
    'color': '#999'
  },
  'text-input:-ms-input-placeholder': {
    'color': '#999'
  },
  'text-input::placeholder': {
    'color': '#999'
  },
  'text-input:disabled::-webkit-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'text-input:disabled::-moz-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'text-input:disabled:-ms-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'text-input:disabled::placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'text-input:invalid': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#1f1f21'
  },
  // ~
  name: Underbar Text Input
  category: Text Input
  elements: ons-input
  markup: |
    <div><input type="text" class="text-input text-input--underbar" placeholder="text" value=""></div>
    <div><input type="text" class="text-input text-input--underbar" placeholder="text" value="" disabled></div>
  'text-input--underbar': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'color': '#1f1f21',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 31 }],
    'fontWeight': '400',
    'boxSizing': 'border-box',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderRadius': '0'
  },
  'text-input--underbar:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }]
  },
  'text-input--underbar:disabled::-webkit-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'text-input--underbar:disabled::-moz-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'text-input--underbar:disabled:-ms-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'text-input--underbar:disabled::placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'text-input--underbar:invalid': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#1f1f21',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }]
  },
  // ~
  name: Material Input
  category: Text Input
  elements: ons-input
  markup: |
    <span>
      <div><input class="text-input text-input--material" placeholder="Username" type="text" required></div>
      <br />
      <div><input class="text-input text-input--material" placeholder="Password" type="password" required></div>
    </span>
  'text-input--material': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': '#212121',
    'backgroundImage': '-webkit-linear-gradient(bottom, transparent 1px, #afafaf 1px)',
    'backgroundImage': 'linear-gradient(to top, transparent 1px, #afafaf 1px)',
    'backgroundSize': '100% 2px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'center bottom',
    'backgroundColor': 'transparent',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'paddingBottom': [{ 'unit': 'px', 'value': 2 }],
    'borderRadius': '0',
    'height': [{ 'unit': 'px', 'value': 24 }],
    'verticalAlign': 'middle',
    'WebkitTransform': 'translate3d(0, 0, 0)',
    // FIXME: prevent ios flicker
  },
  'text-input--material__label': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': '#afafaf',
    'position': 'absolute',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 2 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'fontWeight': '400',
    'pointerEvents': 'none'
  },
  'text-input--material__label--active': {
    'color': '#009688',
    'WebkitTransform': 'translate(0, -75%) scale(0.75)',
    'transform': 'translate(0, -75%) scale(0.75)',
    'WebkitTransformOrigin': 'left top',
    'transformOrigin': 'left top',
    'WebkitTransition': 'color 0.1s ease-in, -webkit-transform 0.1s ease-in',
    'transition': 'color 0.1s ease-in, -webkit-transform 0.1s ease-in',
    'transition': 'transform 0.1s ease-in, color 0.1s ease-in',
    'transition': 'transform 0.1s ease-in, color 0.1s ease-in, -webkit-transform 0.1s ease-in'
  },
  'text-input--material:focus': {
    'backgroundImage': '-webkit-linear-gradient(#009688, #009688),
    -webkit-linear-gradient(bottom, transparent 1px, #afafaf 1px)',
    'backgroundImage': 'linear-gradient(#009688, #009688),
    linear-gradient(to top, transparent 1px, #afafaf 1px)',
    'WebkitAnimation': 'material-text-input-animate 0.3s forwards',
    'animation': 'material-text-input-animate 0.3s forwards'
  },
  'text-input--material::-webkit-input-placeholder': {
    'color': '#afafaf',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'text-input--material::-moz-placeholder': {
    'color': '#afafaf',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'text-input--material:-ms-input-placeholder': {
    'color': '#afafaf',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'text-input--material::placeholder': {
    'color': '#afafaf',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  // ~
  name: Transparent Text Input
  category: Text Input
  elements: ons-input
  markup: |
    <div><input type="text" class="text-input text-input--transparent" placeholder="text" value=""></div>
    <div><input type="text" class="text-input text-input--transparent" placeholder="text" value="" disabled></div>
  'text-input--transparent': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'color': '#1f1f21',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 31 }],
    'fontWeight': '400',
    'boxSizing': 'border-box'
  },
  'text-input--transparent:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  // ~
  name: Textarea
  category: Textarea
  markup: |
    <textarea class="textarea" rows="3" placeholder="Textarea"></textarea>
  'textarea': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'verticalAlign': 'top',
    'resize': 'none',
    'outline': 'none',
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }],
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'fontWeight': '400',
    'borderRadius': '4px',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'backgroundColor': '#f9f9f9',
    'color': '#1f1f21',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'width': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'textarea:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'textarea::-webkit-input-placeholder': {
    'color': '#999'
  },
  'textarea::-moz-placeholder': {
    'color': '#999'
  },
  'textarea:-ms-input-placeholder': {
    'color': '#999'
  },
  'textarea::placeholder': {
    'color': '#999'
  },
  // ~
  name: Textarea Transparent
  category: Textarea
  markup: |
    <textarea class="textarea--transparent" rows="3" placeholder="Textarea"></textarea>
  'textarea--transparent': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'verticalAlign': 'top',
    'resize': 'none',
    'outline': 'none',
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'fontWeight': '400',
    'borderRadius': '4px',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#1f1f21',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'width': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'textarea--transparent:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'textarea--transparent::-webkit-input-placeholder': {
    'color': '#999'
  },
  'textarea--transparent::-moz-placeholder': {
    'color': '#999'
  },
  'textarea--transparent:-ms-input-placeholder': {
    'color': '#999'
  },
  'textarea--transparent::placeholder': {
    'color': '#999'
  },
  // ~
  name: Dialog
  category: Dialog
  elements: ons-dialog
  markup: |
    <div class="dialog-mask"></div>
    <div class="dialog">
      <div class="dialog-container">
        <div class="page">
          <p style="text-align:center;margin-top:40px;opacity:0.4;">Content</p>
        </div>
      </div>
    </div>
  'dialog': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'WebkitTransform': 'translate(-50%, -50%)',
    'transform': 'translate(-50%, -50%)',
    'margin': [{ 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }],
    'overflow': 'hidden',
    'minWidth': [{ 'unit': 'px', 'value': 270 }],
    'minHeight': [{ 'unit': 'px', 'value': 100 }],
    'textAlign': 'left'
  },
  'dialog-container': {
    'height': [{ 'unit': 'string', 'value': 'inherit' }],
    'minHeight': [{ 'unit': 'string', 'value': 'inherit' }],
    'overflow': 'hidden',
    'borderRadius': '4px',
    'backgroundColor': '#f4f4f4',
    'WebkitMaskImage': 'url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC')'
  },
  'dialog-mask': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'rgba(0, 0, 0, .2)'
  },
  // ~
  name: Material Dialog
  category: Dialog
  elements: ons-dialog
  markup: |
    <div class="dialog-mask dialog-mask--material"></div>
    <div class="dialog dialog--material">
      <div class="dialog dialog-container--material">
        <p style="margin-left:24px;margin-right:24px">The quick brown fox jumps over the lazy dog.</p>
      </div>
    </div>
  'dialog--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'textAlign': 'left',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': -5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }]
  },
  'dialog-container--material': {
    'borderRadius': '2px',
    'backgroundColor': '#ffffff'
  },
  'dialog-mask--material': {
    'backgroundColor': 'rgba(0, 0, 0, .3)'
  },
  // ~
  name: Alert Dialog
  category: Alert Dialog
  elements: ons-alert-dialog
  markup: |
    <div class="alert-dialog-mask"></div>
    <div class="alert-dialog">
      <div class="alert-dialog-container">
        <div class="alert-dialog-title">Alert</div>

        <div class="alert-dialog-content">
          Hello World!
        </div>

        <div class="alert-dialog-footer">
          <button class="alert-dialog-button alert-dialog-button--primal">OK</button>
        </div>
      </div>
    </div>
  'alert-dialog': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'WebkitTransform': 'translate(-50%, -50%)',
    'transform': 'translate(-50%, -50%)',
    'width': [{ 'unit': 'px', 'value': 270 }],
    'margin': [{ 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }],
    'backgroundColor': '#f4f4f4',
    'borderRadius': '8px',
    'overflow': 'visible',
    'maxWidth': [{ 'unit': '%H', 'value': 0.95 }],
    'color': '#1f1f21'
  },
  'alert-dialog-container': {
    'height': [{ 'unit': 'string', 'value': 'inherit' }],
    'paddingTop': [{ 'unit': 'px', 'value': 16 }],
    'overflow': 'hidden'
  },
  'alert-dialog-title': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 17 }],
    'fontWeight': '500',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }],
    'textAlign': 'center',
    'color': '#1f1f21'
  },
  'alert-dialog-content': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 12 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'minHeight': [{ 'unit': 'px', 'value': 36 }],
    'textAlign': 'center',
    'color': '#1f1f21'
  },
  'alert-dialog-footer': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'alert-dialog-button': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'textDecoration': 'none',
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'verticalAlign': 'middle',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'backgroundColor': 'transparent',
    'textAlign': 'center',
    'height': [{ 'unit': 'px', 'value': 44 }],
    'lineHeight': [{ 'unit': 'px', 'value': 44 }],
    'outline': 'none',
    'color': 'rgba(24, 103, 194, .81)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderTop': [{ 'unit': 'string', 'value': 'none' }],
      'backgroundSize': '100% 1px',
      'backgroundRepeat': 'no-repeat',
      'backgroundPosition': 'top',
      'backgroundImage': '-webkit-linear-gradient(top, #ddd, #ddd 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(180deg, #ddd, #ddd 50%, transparent 50%)'
    }
  },
  'alert-dialog-button:active': {
    'backgroundColor': 'rgba(0, 0, 0, .05)'
  },
  'alert-dialog-button--primal': {
    'fontWeight': '500'
  },
  'alert-dialog-footer--rowfooter': {
    'whiteSpace': 'nowrap',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'MsFlexWrap': 'wrap',
    'flexWrap': 'wrap'
  },
  'alert-dialog-button--rowfooter': {
    'WebkitBoxFlex': '1',
    'MsFlex': '1',
    'flex': '1',
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'borderLeft': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderTop': [{ 'unit': 'string', 'value': 'none' }],
      'borderLeft': [{ 'unit': 'string', 'value': 'none' }],
      'backgroundSize': '100% 1px, 1px 100%',
      'backgroundRepeat': 'no-repeat',
      'backgroundPosition': 'top, left',
      'backgroundImage': '-webkit-linear-gradient(bottom, transparent, transparent 50%, #ddd 50%),
      -webkit-linear-gradient(left, transparent, transparent 50%, #ddd 50%)',
      'backgroundImage': 'linear-gradient(0deg, transparent, transparent 50%, #ddd 50%),
      linear-gradient(90deg, transparent, transparent 50%, #ddd 50%)'
    }
  },
  'alert-dialog-button--rowfooter:first-child': {
    'borderLeft': [{ 'unit': 'string', 'value': 'none' }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'borderTop': [{ 'unit': 'string', 'value': 'none' }],
      'backgroundSize': '100% 1px',
      'backgroundRepeat': 'no-repeat',
      'backgroundPosition': 'top, left',
      'backgroundImage': '-webkit-linear-gradient(bottom, transparent, transparent 50%, #ddd 50%)',
      'backgroundImage': 'linear-gradient(0deg, transparent, transparent 50%, #ddd 50%)'
    }
  },
  'alert-dialog-mask': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'rgba(0, 0, 0, .2)'
  },
  // ~
  name: Alert Dialog without Title
  category: Alert Dialog
  elements: ons-alert-dialog
  markup: |
    <div class="alert-dialog-mask"></div>
    <div class="alert-dialog">
      <div class="alert-dialog-container">
        <div class="alert-dialog-content">
          Hello World!
        </div>

        <div class="alert-dialog-footer">
          <button class="alert-dialog-button alert-dialog-button--primal">OK</button>
        </div>
      </div>
    </div>
  // ~
  name: Alert Dialog with Multiple Buttons
  category: Alert Dialog
  elements: ons-alert-dialog
  markup: |
    <div class="alert-dialog-mask"></div>
    <div class="alert-dialog">
      <div class="alert-dialog-container">
        <div class="alert-dialog-content">
          Hello World!
        </div>

        <div class="alert-dialog-footer">
          <button class="alert-dialog-button">Cancel</button>
          <button class="alert-dialog-button alert-dialog-button--primal">OK</button>
        </div>
      </div>
    </div>
  // ~
  name: Alert Dialog with Multiple Buttons 2
  category: Alert Dialog
  elements: ons-alert-dialog
  markup: |
    <div class="alert-dialog-mask"></div>
    <div class="alert-dialog">
      <div class="alert-dialog-container">
        <div class="alert-dialog-title">Alert</div>

        <div class="alert-dialog-content">
          Hello World!
        </div>

        <div class="alert-dialog-footer alert-dialog-footer--rowfooter">
          <button class="alert-dialog-button alert-dialog-button--rowfooter">Left</button>
          <button class="alert-dialog-button alert-dialog-button--primal alert-dialog-button--rowfooter">Center</button>
          <button class="alert-dialog-button alert-dialog-button--rowfooter">Right</button>
        </div>
      </div>
    </div>
  // ~
  name: Material Alert Dialog
  category: Alert Dialog
  elements: ons-alert-dialog
  markup: |
    <div class="alert-dialog-mask alert-dialog-mask--material"></div>
    <div class="alert-dialog alert-dialog--material">
      <div class="alert-dialog-container alert-dialog-container--material">
        <div class="alert-dialog-title alert-dialog-title--material">
          Dialog title
        </div>
        <div class="alert-dialog-content alert-dialog-content--material">
          Some dialog content.
        </div>
        <div class="alert-dialog-footer alert-dialog-footer--material">
          <button class="alert-dialog-button alert-dialog-button--material">OK</button>
          <button class="alert-dialog-button alert-dialog-button--material">CANCEL</button>
        </div>
      </div>
    </div>
  'alert-dialog--material': {
    'borderRadius': '2px',
    'backgroundColor': '#ffffff'
  },
  'alert-dialog-container--material': {
    'paddingTop': [{ 'unit': 'px', 'value': 22 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': -5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }]
  },
  'alert-dialog-title--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'textAlign': 'left',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'fontWeight': '500',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }],
    'color': '#212121'
  },
  'alert-dialog-content--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'textAlign': 'left',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }],
    'margin': [{ 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }],
    'minHeight': [{ 'unit': 'px', 'value': 0 }],
    'color': '#727272'
  },
  'alert-dialog-footer--material': {
    'display': 'inline-block',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }],
    'boxSizing': 'border-box'
  },
  'alert-dialog-button--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    // text-transform: uppercase;
    'display': 'inline-block',
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'minWidth': [{ 'unit': 'px', 'value': 70 }],
    'float': 'right',
    'background': 'none',
    'borderTop': [{ 'unit': 'string', 'value': 'none' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'fontWeight': '500',
    'outline': 'none',
    'color': '#009688',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'background': 'none'
    }
  },
  'alert-dialog-button--material:active': {
    'backgroundColor': 'transparent',
    'backgroundColor': 'initial'
  },
  'alert-dialog-button--rowfooter--material': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'background': 'none'
    }
  },
  'alert-dialog-button--rowfooter--material:first-child': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'background': 'none'
    }
  },
  'alert-dialog-button--primal--material': {
    'fontWeight': '500',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'background': 'none'
    }
  },
  'alert-dialog-mask--material': {
    'backgroundColor': 'rgba(0, 0, 0, .3)'
  },
  // ~
  name: Popover
  category: Popover
  elements: ons-popover
  markup: |
    <div class="popover-mask"></div>
    <div class="popover popover--bottom" style="bottom: 20px; left: 65px;">
      <div class="popover__arrow popover--bottom__arrow" style="left: 110px;"></div>
      <div class="popover__content popover--bottom__content">
        <div style="text-align:center;opacity:0.8;margin-top:40px">Content</div>
      </div>
    </div>
  // ~
  name: Popover(top)
  category: Popover
  elements: ons-popover
  markup: |
    <div class="popover-mask"></div>
    <div class="popover popover--top" style="top: 20px; left: 50px;">
      <div class="popover__arrow popover--top__arrow" style="margin-left: 110px;"></div>
      <div class="popover__content popover--top__content">
        <div style="text-align: center; opacity: 0.8; margin-top: 40px">Content</div>
      </div>
    </div>
  // ~
  name: Popover(left)
  category: Popover
  elements: ons-popover
  markup: |
    <div class="popover-mask"></div>
    <div class="popover popover--right" style="top: 20px; right: 20px;">
      <div class="popover__arrow popover--right__arrow" style="bottom: 50px;"></div>
      <div class="popover__content popover--right__content">
        <div style="text-align: center; opacity: 0.8; margin-top: 40px">Content</div>
      </div>
    </div>
  // ~
  name: Popover(right)
  category: Popover
  elements: ons-popover
  markup: |
    <div class="popover-mask"></div>
    <div class="popover popover--left" style="top: 20px;left: 20px;">
      <div class="popover__arrow popover--left__arrow" style="top: 50px;"></div>
      <div class="popover__content popover--left__content">
        <div style="text-align: center; opacity: 0.8; margin-top: 40px">Content</div>
      </div>
    </div>
  'popover': {
    'position': 'absolute',
    'zIndex': '20001'
  },
  'popover--bottom': {
    'bottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'popover--top': {
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'popover--left': {
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'popover--right': {
    'right': [{ 'unit': 'px', 'value': 0 }]
  },
  'popover-mask': {
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'rgba(0, 0, 0, .2)',
    'position': 'absolute',
    'zIndex': '19999'
  },
  'popover__content': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 220 }],
    'overflow': 'auto',
    'minHeight': [{ 'unit': 'px', 'value': 100 }],
    'maxHeight': [{ 'unit': '%V', 'value': 1 }],
    'backgroundColor': 'white',
    'borderRadius': '8px',
    'color': '#1f1f21',
    'pointerEvents': 'auto'
  },
  'popover--top__content': {
  },
  'popover--bottom__content': {
  },
  'popover--left__content': {
  },
  'popover--right__content': {
  },
  'popover__arrow': {
    'position': 'absolute',
    'width': [{ 'unit': 'px', 'value': 18 }],
    'height': [{ 'unit': 'px', 'value': 18 }],
    'WebkitTransformOrigin': '50% 50% 0',
    'transformOrigin': '50% 50% 0',
    'backgroundColor': 'transparent',
    'backgroundImage': '-webkit-linear-gradient(45deg, white, white 50%, transparent 50%)',
    'backgroundImage': 'linear-gradient(45deg, white, white 50%, transparent 50%)',
    'borderRadius': '0 0 0 4px',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'zIndex': '20001'
  },
  // NOTE: If you changed this properties, you should check if ons-popover is broken.
  'popover--bottom__arrow': {
    'WebkitTransform': 'translateY(6px) translateX(-9px) rotate(-45deg)',
    'transform': 'translateY(6px) translateX(-9px) rotate(-45deg)',
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'marginRight': [{ 'unit': 'px', 'value': -18 }]
  },
  'popover--top__arrow': {
    'WebkitTransform': 'translateY(-6px) translateX(-9px) rotate(135deg)',
    'transform': 'translateY(-6px) translateX(-9px) rotate(135deg)',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'marginRight': [{ 'unit': 'px', 'value': -18 }]
  },
  'popover--left__arrow': {
    'WebkitTransform': 'translateX(-6px) translateY(-9px) rotate(45deg)',
    'transform': 'translateX(-6px) translateY(-9px) rotate(45deg)',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': -18 }]
  },
  'popover--right__arrow': {
    'WebkitTransform': 'translateX(6px) translateY(-9px) rotate(225deg)',
    'transform': 'translateX(6px) translateY(-9px) rotate(225deg)',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': -18 }]
  },
  // ~
  name: Material Popover
  category: Popover
  elements: ons-popover
  markup: |
    <div class="popover-mask popover-mask--material"></div>
    <div class="popover popover--material popover--left" style="top: 50px; left: 65px;">
      <div class="popover__arrow popover--material__arrow popover--left__arrow"></div>
      <div class="popover__content popover--material__content popover--left__content">
        <div style="text-align: center; opacity: 0.8; margin-top: 40px">Content</div>
      </div>
    </div>
  'popover--material': {
  },
  'popover-mask--material': {
    'backgroundColor': 'transparent'
  },
  'popover--material__content': {
    'backgroundColor': '#fafafa',
    'borderRadius': '2px',
    'color': '#1f1f21',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }]
  },
  'popover--material__arrow': {
    'display': 'none'
  },
  // ~
  name: Progress Bar
  category: Progress Bar
  elements: ons-progress-bar
  markup: |
    <div class="progress-bar">
      <div class="progress-bar__primary" style="width: 30%"></div>
    </div>
    <br />
    <div class="progress-bar">
      <div class="progress-bar__primary" style="width:20%"></div>
      <div class="progress-bar__secondary" style="width:76%"></div>
    </div>
    <br />
    <div class="progress-bar progress-bar--indeterminate">
    </div>
  'progress-bar': {
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 4 }],
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'backgroundColor': '#e0e0e0',
    'backgroundClip': 'padding-box',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'hidden'
  },
  'progress-bar__primary': {
    'position': 'absolute',
    'backgroundColor': '#009688',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'WebkitTransition': 'width .3s linear',
    'transition': 'width .3s linear',
    'zIndex': '100'
  },
  'progress-bar__secondary': {
    'position': 'absolute',
    'backgroundColor': '#009688',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'WebkitTransition': 'width .3s linear',
    'transition': 'width .3s linear',
    'zIndex': '100'
  },
  'progress-bar__secondary': {
    'backgroundColor': '#80cbc4',
    'zIndex': '0'
  },
  'progress-bar--indeterminate:before': {
    'content': '''',
    'position': 'absolute',
    'backgroundColor': '#009688',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'willChange': 'left, right',
    'WebkitAnimation': 'progress-bar__indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite',
    'animation': 'progress-bar__indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite'
  },
  'progress-bar--indeterminate:after': {
    'content': '''',
    'position': 'absolute',
    'backgroundColor': '#009688',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'willChange': 'left, right',
    'WebkitAnimation': 'progress-bar__indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite',
    'animation': 'progress-bar__indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite',
    'WebkitAnimationDelay': '1.15s',
    'animationDelay': '1.15s'
  },
  // ~
  name: Progress Circle
  category: Progress Circle
  elements: ons-progress-circular
  markup: |
    <svg class="progress-circular progress-circular--indeterminate">
      <circle class="progress-circular__primary progress-circular--indeterminate__primary"/>
      <circle class="progress-circular__secondary progress-circular--indeterminate__secondary"/>
    </svg>

    <svg class="progress-circular">
      <circle class="progress-circular__secondary" style="stroke-dasharray: 140%, 251.32%"/>
      <circle class="progress-circular__primary" style="stroke-dasharray: 100%, 251.32%"/>
    </svg>

    <svg class="progress-circular">
      <circle class="progress-circular__primary" style="stroke-dasharray: 80%, 251.32%"/>
    </svg>
  'progress-circular': {
    'height': [{ 'unit': 'px', 'value': 32 }],
    'position': 'relative',
    'width': [{ 'unit': 'px', 'value': 32 }],
    'WebkitTransform': 'rotate(270deg)',
    'transform': 'rotate(270deg)',
    'WebkitAnimation': 'none',
    'animation': 'none'
  },
  'progress-circular__primary': {
    'cx': '50%',
    'cy': '50%',
    'r': '40%',
    'WebkitAnimation': 'none',
    'animation': 'none',
    'fill': 'none',
    'strokeWidth': '9%',
    'strokeMiterlimit': '10'
  },
  'progress-circular__secondary': {
    'cx': '50%',
    'cy': '50%',
    'r': '40%',
    'WebkitAnimation': 'none',
    'animation': 'none',
    'fill': 'none',
    'strokeWidth': '9%',
    'strokeMiterlimit': '10'
  },
  'progress-circular__primary': {
    'strokeDasharray': '1, 200',
    'strokeDashoffset': '0',
    'stroke': '#009688',
    'WebkitTransition': 'all 1s cubic-bezier(0.4, 0, 0.2, 1)',
    'transition': 'all 1s cubic-bezier(0.4, 0, 0.2, 1)'
  },
  'progress-circular__secondary': {
    'stroke': '#80cbc4'
  },
  'progress-circular--indeterminate': {
    'WebkitAnimation': 'progress__rotate 2s linear infinite',
    'animation': 'progress__rotate 2s linear infinite',
    'WebkitTransform': 'none',
    'transform': 'none'
  },
  'progress-circular--indeterminate__primary': {
    'WebkitAnimation': 'progress__dash 1.5s ease-in-out infinite',
    'animation': 'progress__dash 1.5s ease-in-out infinite'
  },
  'progress-circular--indeterminate__secondary': {
    'display': 'none'
  },
  // ~
  name: Fab
  category: Fab
  elements: ons-fab
  markup: |
    <button class="fab"><i class="zmdi zmdi-car"></i></button>
    <button class="fab" disabled><i class="zmdi zmdi-car"></i></button>
  'fab': {
    'position': 'relative',
    'display': 'inline-block',
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none',
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'width': [{ 'unit': 'px', 'value': 56 }],
    'height': [{ 'unit': 'px', 'value': 56 }],
    'textDecoration': 'none',
    'fontSize': [{ 'unit': 'px', 'value': 25 }],
    'lineHeight': [{ 'unit': 'px', 'value': 56 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0 }],
    'color': '#ffffff',
    'verticalAlign': 'middle',
    'textAlign': 'center',
    'backgroundColor': '#009688',
    'border': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'currentColor' }],
    'borderRadius': '50%',
    'overflow': 'hidden',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }],
    'WebkitTransition': 'all 0.2s ease-in-out',
    'transition': 'all 0.2s ease-in-out'
  },
  'fab:active': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': -3 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }],
    'backgroundColor': '#009688',
    'WebkitTransition': 'all 0.2s ease',
    'transition': 'all 0.2s ease'
  },
  'fab:focus': {
    'outline': '0'
  },
  'fab__icon': {
    'position': 'relative',
    'overflow': 'hidden',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'display': 'block',
    'borderRadius': '100%',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'zIndex': '100',
    'lineHeight': [{ 'unit': 'px', 'value': 56 }]
  },
  'fab:disabled': {
    'backgroundColor': 'alpha(black, 0.5)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'fab[disabled]': {
    'backgroundColor': 'alpha(black, 0.5)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none'
  },
  'fab--top__right': {
    'top': [{ 'unit': 'px', 'value': 20 }],
    'bottom': [{ 'unit': 'string', 'value': 'auto' }],
    'right': [{ 'unit': 'px', 'value': 20 }],
    'left': [{ 'unit': 'string', 'value': 'auto' }],
    'position': 'absolute'
  },
  'fab--bottom__right': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'bottom': [{ 'unit': 'px', 'value': 20 }],
    'right': [{ 'unit': 'px', 'value': 20 }],
    'left': [{ 'unit': 'string', 'value': 'auto' }],
    'position': 'absolute'
  },
  'fab--top__left': {
    'top': [{ 'unit': 'px', 'value': 20 }],
    'bottom': [{ 'unit': 'string', 'value': 'auto' }],
    'right': [{ 'unit': 'string', 'value': 'auto' }],
    'left': [{ 'unit': 'px', 'value': 20 }],
    'position': 'absolute'
  },
  'fab--bottom__left': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'bottom': [{ 'unit': 'px', 'value': 20 }],
    'right': [{ 'unit': 'string', 'value': 'auto' }],
    'left': [{ 'unit': 'px', 'value': 20 }],
    'position': 'absolute'
  },
  'fab--top__center': {
    'top': [{ 'unit': 'px', 'value': 20 }],
    'bottom': [{ 'unit': 'string', 'value': 'auto' }],
    'marginLeft': [{ 'unit': 'px', 'value': -28 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'right': [{ 'unit': 'string', 'value': 'auto' }],
    'position': 'absolute'
  },
  'fab--bottom__center': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'bottom': [{ 'unit': 'px', 'value': 20 }],
    'marginLeft': [{ 'unit': 'px', 'value': -28 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'right': [{ 'unit': 'string', 'value': 'auto' }],
    'position': 'absolute'
  },
  // ~
  name: Fab Mini
  category: Fab
  elements: ons-fab
  markup: |
    <button class="fab fab--mini"><i class="zmdi zmdi-plus"></i></button>
    <button class="fab fab--mini" disabled><i class="zmdi zmdi-plus"></i></button>
  'fab--mini': {
    'width': [{ 'unit': 'px', 'value': 40 }],
    'height': [{ 'unit': 'px', 'value': 40 }],
    'lineHeight': [{ 'unit': 'px', 'value': 40 }]
  },
  'fab--mini__icon': {
    'lineHeight': [{ 'unit': 'px', 'value': 40 }]
  },
  'speed-dial__item': {
    'position': 'absolute',
    'WebkitTransform': 'scale(0)',
    'transform': 'scale(0)'
  },
  // ~
  name: Modal
  category: Modal
  elements: ons-modal
  markup: |
    <div class="modal">
      <div class="modal__content">
        Message Text
      </div>
    </div>
  'modal': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'wordSpacing': '0',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'overflow': 'hidden',
    'backgroundColor': 'rgba(0, 0, 0, .7)',
    'position': 'absolute',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'display': 'table',
    'zIndex': '2147483647'
  },
  'modal__content': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'wordSpacing': '0',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'display': 'table-cell',
    'verticalAlign': 'middle',
    'textAlign': 'center',
    'color': '#fff',
    'whiteSpace': 'normal'
  },
  // ~
  name: Select Input
  category: Select Input
  markup: |
    <select class="select-input">
      <option>Option 1</option>
      <option>Option 2</option>
      <option>Option 3</option>
    </select>
  'select-input': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'position': 'relative',
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 31 }],
    'borderColor': '#ccc',
    'color': 'rgba(24, 103, 194, .81)'
  },
  'select-input::-ms-clear': {
    'display': 'none'
  },
  'select-input::-webkit-input-placeholder': {
    'color': '#999'
  },
  'select-input::-moz-placeholder': {
    'color': '#999'
  },
  'select-input:-ms-input-placeholder': {
    'color': '#999'
  },
  'select-input::placeholder': {
    'color': '#999'
  },
  'select-input:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'select-input:disabled::-webkit-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'select-input:disabled::-moz-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'select-input:disabled:-ms-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'select-input:disabled::placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999'
  },
  'select-input:invalid': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#1f1f21'
  },
  'select-input[multiple]': {
    'height': [{ 'unit': 'px', 'value': 62 }]
  },
  // ~
  name: Material Select Input
  category: Select Input
  markup: |
    <select class="select-input select-input--material">
      <option>Option 1</option>
      <option>Option 2</option>
      <option>Option 3</option>
    </select>
  'select-input--material': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': 'rgba(0, 0, 0, 1)',
    'backgroundImage': '-webkit-linear-gradient(bottom, transparent 1px, rgba(0, 0, 0, .81) 1px)',
    'backgroundImage': 'linear-gradient(to top, transparent 1px, rgba(0, 0, 0, .81) 1px)',
    'backgroundSize': '100% 2px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'center bottom',
    'backgroundColor': 'transparent',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'fontWeight': '400',
    'paddingBottom': [{ 'unit': 'px', 'value': 2 }],
    'borderRadius': '0',
    'height': [{ 'unit': 'px', 'value': 24 }],
    'WebkitTransform': 'translate3d(0, 0, 0)',
    'transform': 'translate3d(0, 0, 0)',
    // prevent ios flicker
    'border': [{ 'unit': 'string', 'value': 'none' }]
  },
  'select-input--material__label': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': 'rgba(0, 0, 0, .81)',
    'position': 'absolute',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 2 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'pointerEvents': 'none'
  },
  'select-input--material__label--active': {
    'color': 'rgba(0, 0, 0, .15)',
    'WebkitTransform': 'translate(0, -75%) scale(0.75)',
    'transform': 'translate(0, -75%) scale(0.75)',
    'WebkitTransformOrigin': 'left top',
    'transformOrigin': 'left top',
    'WebkitTransition': 'color 0.1s ease-in, -webkit-transform 0.1s ease-in',
    'transition': 'color 0.1s ease-in, -webkit-transform 0.1s ease-in',
    'transition': 'transform 0.1s ease-in, color 0.1s ease-in',
    'transition': 'transform 0.1s ease-in, color 0.1s ease-in, -webkit-transform 0.1s ease-in'
  },
  'select-input--material:focus': {
    'backgroundImage': '-webkit-linear-gradient(rgba(0, 0, 0, .15), rgba(0, 0, 0, .15)), -webkit-linear-gradient(bottom, transparent 1px, rgba(0, 0, 0, .81) 1px)',
    'backgroundImage': 'linear-gradient(rgba(0, 0, 0, .15), rgba(0, 0, 0, .15)), linear-gradient(to top, transparent 1px, rgba(0, 0, 0, .81) 1px)',
    'WebkitAnimation': 'material-select-input-animate 0.3s forwards',
    'animation': 'material-select-input-animate 0.3s forwards',
    'borderRadius': '0'
  },
  'select-input--material::-webkit-input-placeholder': {
    'color': 'rgba(0, 0, 0, .81)',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'select-input--material::-moz-placeholder': {
    'color': 'rgba(0, 0, 0, .81)',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'select-input--material:-ms-input-placeholder': {
    'color': 'rgba(0, 0, 0, .81)',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  'select-input--material::placeholder': {
    'color': 'rgba(0, 0, 0, .81)',
    'lineHeight': [{ 'unit': 'px', 'value': 20 }]
  },
  // ~
  name: Underbar Select Input
  category: Select Input
  markup: |
    <select class="select-input select-input--underbar">
      <option>Option 1</option>
      <option>Option 2</option>
      <option>Option 3</option>
    </select>
  'select-input--underbar': {
    'boxSizing': 'border-box',
    'backgroundClip': 'padding-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'background': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'verticalAlign': 'top',
    'outline': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'position': 'relative',
    'fontSize': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 31 }],
    'borderColor': '#ccc',
    'color': 'rgba(24, 103, 194, .81)',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderRadius': '0'
  },
  'select-input--underbar:disabled': {
    'opacity': '0.3',
    'cursor': 'default',
    'pointerEvents': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }]
  },
  'select-input--underbar:disabled::-webkit-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'select-input--underbar:disabled::-moz-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'select-input--underbar:disabled:-ms-input-placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'select-input--underbar:disabled::placeholder': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#999',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent'
  },
  'select-input--underbar:invalid': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'color': '#1f1f21',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }]
  },
  // ~
  name: Action Sheet
  category: Action Sheet
  markup: |
    <div class="action-sheet-mask"></div>
    <div class="action-sheet">
      <div class="action-sheet-title">Title</div>
      <button class="action-sheet-button">Label</button>
      <button class="action-sheet-button">Cancel</button>
    </div>
  // ~
  name: Action Sheet with Delete Label
  category: Action Sheet
  markup: |
    <div class="action-sheet-mask"></div>
    <div class="action-sheet">
      <button class="action-sheet-button">Label</button>
      <button class="action-sheet-button action-sheet-button--destructive">Delete Label</button>
      <button class="action-sheet-button">Cancel</button>
    </div>
  'action-sheet': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'cursor': 'default',
    'position': 'absolute',
    'left': [{ 'unit': 'px', 'value': 9 }],
    'right': [{ 'unit': 'px', 'value': 9 }],
    'bottom': [{ 'unit': 'px', 'value': 7 }],
    'zIndex': '2'
  },
  'action-sheet-button': {
    'boxSizing': 'border-box',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'textAlign': 'center',
    'color': 'rgba(24, 103, 194, .81)',
    'backgroundColor': 'rgba(255, 255, 255, .9)',
    'borderRadius': '0',
    'lineHeight': [{ 'unit': 'px', 'value': 56 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 100%)',
    'backgroundImage': 'linear-gradient(0deg, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 50%, transparent 50%)'
    }
  },
  'action-sheet-button:first-child': {
    'borderTopLeftRadius': '12px',
    'borderTopRightRadius': '12px'
  },
  'action-sheet-button:active': {
    'backgroundColor': '#e9e9e9',
    'backgroundImage': 'none'
  },
  'action-sheet-button:focus': {
    'outline': 'none'
  },
  'action-sheet-button:nth-last-of-type(2)': {
    'borderBottomRightRadius': '12px',
    'borderBottomLeftRadius': '12px',
    'backgroundImage': 'none'
  },
  'action-sheet-button:last-of-type': {
    'borderRadius': '12px',
    'margin': [{ 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'backgroundColor': '#fff',
    'backgroundImage': 'none',
    'fontWeight': '600'
  },
  'action-sheet-button:last-of-type:active': {
    'backgroundColor': '#e9e9e9'
  },
  'action-sheet-button--destructive': {
    'color': '#fe3824'
  },
  'action-sheet-title': {
    'boxSizing': 'border-box',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'color': '#8f8e94',
    'textAlign': 'center',
    'backgroundColor': 'rgba(255, 255, 255, .9)',
    'lineHeight': [{ 'unit': 'px', 'value': 56 }],
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden',
    'backgroundSize': '100% 1px',
    'backgroundRepeat': 'no-repeat',
    'backgroundPosition': 'bottom',
    'backgroundImage': '-webkit-linear-gradient(bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 100%)',
    'backgroundImage': 'linear-gradient(0deg, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 100%)',
    '-webkit-min-device-pixel-ratio||min-resolution 192dp': {
      'backgroundImage': '-webkit-linear-gradient(bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 50%, transparent 50%)',
      'backgroundImage': 'linear-gradient(0deg, rgba(0, 0, 0, .1), rgba(0, 0, 0, .1) 50%, transparent 50%)'
    }
  },
  'action-sheet-title:first-child': {
    'borderTopLeftRadius': '12px',
    'borderTopRightRadius': '12px'
  },
  'action-sheet-icon': {
    'display': 'none'
  },
  'action-sheet-mask': {
    'position': 'absolute',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'rgba(0, 0, 0, .1)',
    'zIndex': '1'
  },
  // ~
  name: Material Action Sheet
  category: Action Sheet
  markup: |
    <div class="action-sheet-mask action-sheet-mask--material"></div>
    <div class="action-sheet action-sheet--material">
      <button class="action-sheet-button action-sheet-button--material"><i class="ion-android-checkbox-blank action-sheet-icon--material"></i>Label</button>
      <button class="action-sheet-button action-sheet-button--material"><i class="ion-android-checkbox-blank action-sheet-icon--material"></i>Label</button>
      <button class="action-sheet-button action-sheet-button--material"><i class="ion-android-close action-sheet-icon--material"></i>Cancel</button>
    </div>
  // ~
  name: Material Action Sheet with Title
  category: Action Sheet
  markup: |
    <div class="action-sheet-mask action-sheet-mask--material"></div>
    <div class="action-sheet action-sheet--material">
      <div class="action-sheet-title action-sheet-title--material">Title</div>
      <button class="action-sheet-button action-sheet-button--material"><i class="ion-android-checkbox-blank action-sheet-icon--material"></i>Label</button>
      <button class="action-sheet-button action-sheet-button--material"><i class="ion-android-close action-sheet-icon--material"></i>Cancel</button>
    </div>
  'action-sheet--material': {
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': -5 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .4)' }]
  },
  'action-sheet-title--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'borderRadius': '0',
    'backgroundImage': 'none',
    'textAlign': 'left',
    'height': [{ 'unit': 'px', 'value': 56 }],
    'lineHeight': [{ 'unit': 'px', 'value': 56 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'color': '#686868',
    'backgroundColor': 'white',
    'fontWeight': '400'
  },
  'action-sheet-title--material:first-child': {
    'borderRadius': '0'
  },
  'action-sheet-button--material': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'borderRadius': '0',
    'backgroundImage': 'none',
    'height': [{ 'unit': 'px', 'value': 52 }],
    'lineHeight': [{ 'unit': 'px', 'value': 52 }],
    'textAlign': 'left',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }],
    'color': '#686868',
    'fontWeight': '400',
    'backgroundColor': 'white'
  },
  'action-sheet-button--material:first-child': {
    'borderRadius': '0'
  },
  'action-sheet-button--material:nth-last-of-type(2)': {
    'borderRadius': '0'
  },
  'action-sheet-button--material:last-of-type': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'borderRadius': '0',
    'fontWeight': '400',
    'backgroundColor': 'white'
  },
  'action-sheet-icon--material': {
    'display': 'inline-block',
    'float': 'left',
    'height': [{ 'unit': 'px', 'value': 52 }],
    'lineHeight': [{ 'unit': 'px', 'value': 52 }],
    'marginRight': [{ 'unit': 'px', 'value': 32 }],
    'fontSize': [{ 'unit': 'px', 'value': 26 }],
    'width': [{ 'unit': 'em', 'value': 0.8 }],
    'textAlign': 'center'
  },
  'action-sheet-mask--material': {
    'backgroundColor': 'rgba(0, 0, 0, .2)'
  },
  // ~
  name: Card
  category: Card
  markup: |
    <div style="background-color: #eee; height: 200px; padding: 1px 0 0 0;">
      <div class="card">
        <div class="card__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
      </div>
    </div>
  'card': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }],
    'borderRadius': '8px',
    'backgroundColor': 'white',
    'boxSizing': 'border-box',
    'display': 'block',
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'padding': [{ 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 16 }],
    'textAlign': 'left'
  },
  'card__content': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.4 }],
    'color': '#030303'
  },
  // ~
  name: Card with Title
  category: Card
  markup: |
    <div style="background-color: #eee; height: 200px; padding: 1px 0 0 0;">
      <div class="card">
        <h2 class="card__title">Card Title</h2>
        <div class="card__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
      </div>
    </div>
  'card__title': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'margin': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'display': 'block',
    'boxSizing': 'border-box'
  },
  // ~
  name: Material Card
  category: Card
  markup: |
    <div style="background-color: #eee; height: 200px; padding: 1px 0 0 0;">
      <div class="card card--material">
        <div class="card__content card--material__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
      </div>
    </div>
  'card--material': {
    'backgroundColor': 'white',
    'borderRadius': '2px',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12),
' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'string', 'value': '' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': -2 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }],
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400'
  },
  'card--material__content': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.4 }],
    'color': 'rgba(0, 0, 0, .54)'
  },
  // ~
  name: Material Card with Title
  category: Card
  markup: |
    <div style="background-color: #eee; height: 200px; padding: 1px 0 0 0;">
      <div class="card card--material">
        <div class="card__title card--material__title">Card Title</div>
        <div class="card__content card--material__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
      </div>
    </div>
  'card--material__title': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 0 }]
  },
  // ~
  name: Toast
  category: Toast
  markup: |
    <div class="toast">
      <div class="toast__message">Message Message Message Message Message Message</div>
      <button class="toast__button">ACTION</button>
    </div>
  'toast': {
    'fontFamily': '-apple-system, 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale',
    'fontWeight': '400',
    'position': 'absolute',
    'zIndex': '2',
    'left': [{ 'unit': 'px', 'value': 8 }],
    'right': [{ 'unit': 'px', 'value': 8 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }],
    'borderRadius': '8px',
    'backgroundColor': 'rgba(0, 0, 0, .8)',
    'display': '-webkit-box',
    'display': '-ms-flexbox',
    'display': 'flex',
    'minHeight': [{ 'unit': 'px', 'value': 48 }],
    'lineHeight': [{ 'unit': 'px', 'value': 48 }],
    'boxSizing': 'border-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }]
  },
  'toast__message': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': 'white',
    'WebkitBoxFlex': '1',
    'MsFlexPositive': '1',
    'flexGrow': '1',
    'textAlign': 'left',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap',
    'overflow': 'hidden'
  },
  'toast__button': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': 'white',
    'WebkitBoxFlex': '0',
    'MsFlexPositive': '0',
    'flexGrow': '0',
    'WebkitAppearance': 'none',
    'MozAppearance': 'none',
    'appearance': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'backgroundColor': 'transparent',
    'cursor': 'default',
    // text-transform: uppercase;
  },
  'toast__button:focus': {
    'outline': 'none'
  },
  'toast__button:active': {
    'opacity': '0.4'
  },
  // ~
  name: Material Toast
  category: Toast
  markup: |
    <div class="toast toast--material">
      <div class="toast__message toast--material__message">Message Message Message Message Message Message</div>
      <button class="toast__button toast--material__button">ACTION</button>
    </div>
  'toast--material': {
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'rgba(0, 0, 0, .8)',
    'borderRadius': '0',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }]
  },
  'toast--material__message': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'toast--material__button': {
    'fontFamily': ''Roboto', 'Noto', sans-serif',
    'WebkitFontSmoothing': 'antialiased',
    'fontWeight': '400',
    'color': '#80cbc4'
  }
});
