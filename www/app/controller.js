angular.module('zyppo.controller', [])
    .controller('CpfValideController', function ($scope, $http) {
        $scope.validarCpf = function () {
            if ($scope.cpf == '' || $scope.cpf == undefined) {
                ons.notification.alert('Por favor preencher o CPF');
                return false;
            }
            modalCpf.show();
            $http.post('https://zypo.herokuapp.com/usuario/verifica-cpf/cpf/' + $scope.cpf).then(function (response) {
                modalCpf.hide();
                if (response.data.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a') == "nao cadastrado") {
                    nav.pushPage('pages/cadastro.html', {data: {cpf: $scope.cpf}});
                } else if (response.data == 'ja cadastrado') {
                    nav.pushPage('pages/login.html', {data: {cpf: $scope.cpf}});
                } else if (response.data.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a') == "O CPF digitado e invalido") {
                    ons.notification.alert('O CPF digitado e invalido');
                }
            })
        }
    })
    .controller('LoginController', ['$scope', '$http', function ($scope, $http) {

        var cpf = nav.topPage.data.cpf;
        $scope.cpf = cpf;


        $scope.login = function () {
            if ($scope.cpf == undefined || $scope.cpf == '') {
                ons.notification.alert('Por favor preencher o CPF');
                return false;
            } else if ($scope.senha == '' || $scope.senha == undefined) {
                ons.notification.alert('Por favor preencher a senha');
                return false;
            }

            modalLogin.show();
            $http.post('https://zypo.herokuapp.com/usuario/login', {
                cpf: $scope.cpf,
                senha: $scope.senha
            }).then(function (response) {
                modalLogin.hide();
                console.log(response)
                $scope.login.senha = '';
                if (response.data.cpf != undefined) {
                    ons.notification.alert('Login efetuado com sucesso');
                } else {
                    ons.notification.alert(response.data);
                }

            })
        }
    }])
    .controller('CadastroController', ['$scope', '$http', function ($scope, $http) {
        var cpf = nav.topPage.data.cpf;
        console.log(cpf)
        $scope.cad = {};
        $scope.pitctureFeminino = 'img/feminino_pb.png';
        $scope.toogleFeminino = function () {
            if ($scope.pitctureFeminino == 'img/feminino_pb.png') {
                $scope.cad.sexo = 'F';
                $scope.pitctureFeminino = 'img/feminino.png';
                $scope.pitctureMasculina = 'img/masculino_pb.png';
                $scope.pictureSem = 'img/semresposta_pb.png';
            }
        }
        $scope.pitctureMasculina = 'img/masculino_pb.png';
        $scope.toogleMasculina = function () {
            if ($scope.pitctureMasculina == 'img/masculino_pb.png') {
                $scope.cad.sexo = 'M';
                $scope.pitctureMasculina = 'img/masculino.png';
                $scope.pitctureFeminino = 'img/feminino_pb.png';
                $scope.pictureSem = 'img/semresposta_pb.png';
            }
        }
        $scope.pictureSem = 'img/semreposta.png';
        $scope.toogleSem = function () {
            // console.log()
            if ($scope.pictureSem == 'img/semresposta_pb.png') {
                $scope.cad.sexo = 'I';
                $scope.pitctureMasculina = 'img/masculino_pb.png';
                $scope.pitctureFeminino = 'img/feminino_pb.png';
                $scope.pictureSem = 'img/semreposta.png';
            }
        }

        $scope.cadastrar = function () {
            if ($scope.cad.nome == undefined || $scope.cad.nome == '') {
                ons.notification.alert('Por favor preencher o Nome');
                return false;
            } else if ($scope.cad.email == '' || $scope.cad.email == undefined) {
                ons.notification.alert('Por favor preencher o seu Email');
                return false;
            } else if ($scope.cad.senha == '' || $scope.cad.senha == undefined) {
                ons.notification.alert('Por favor preencher o sua Senha');
                return false;
            } else if ($scope.cad.senha != $scope.cad.confirmsenha) {
                ons.notification.alert('As senhas não conferem');
                return false;
            } else if ($scope.cad.data_nascimento == undefined) {
                ons.notification.alert('Escolha a data de nascimento');
                return false;
            }
            $scope.cad.cpf = cpf;
            // $scope.cad.data_nascimento = $scope.cad.data_nascimento.substr(0,10);
            console.log($scope.cad)
            modalCad.show();
            $http.post('https://zypo.herokuapp.com/usuario/save', $scope.cad).then(function (response) {
                modalCad.hide();
                console.log(response)
                $scope.cad = {
                    nome: '',
                    email: '',
                    senha: '',
                    confirmsenha: '',
                    data_nascimento: '',

                };
                ons.notification.alert('Cadastro realizado com Sucesso');
            })
        }
    }])